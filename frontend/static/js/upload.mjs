// --- IMPORTS ---------------------------------------------------------------------------------------------------------
import { create } from "./basics.js";
import {get_base} from "./config.js";

// --- GLOBAL VARIABLES ------------------------------------------------------------------------------------------------
let all_files = new Set();
let productions = [];
let measurements = [];
let failed_uploads = 0;
let success_uploads = 0;
let read_files = [];

// --- INITIALIZATION --------------------------------------------------------------------------------------------------
export function load_content(main_content, footer, title) {
    // --- variables ---
    const cb = "<div class='content_block'></div>";
    const upload_button = create("<button id='upload_button'>Upload</button>");
    const measurement_input = create("<input id='measurement_input' type='text'>");
    const production_input = create("<input id='production_input' type='text'>");
    const top_span = create("<span id='top_span'></span>");
    const browse_span = create("<span id='browse_span'>Please select files (Ctrl + A)</span>");
    const browse_button = create("<button id='browse_button'>Browse</button>");
    const file_input = create("<input id='file_input' type='file' style='display: none;' multiple>");
    const progress_div = create("<div id='progress_div' class='content_block'></div>");

    // --- construct the body ---
    footer.appendChild(upload_button);
    if(main_content) {
        main_content.appendChild(create("<div class='container'></div>", [
            create(cb, [top_span]),
            create(cb, [
                "<label>Production:</label>",
                production_input
            ]),
            create(cb,[
                "<label>Measurement:</label>",
                measurement_input
            ]),
            create(cb, [
                "<label>Files:</label>",
                browse_span,
                browse_button,
                file_input
            ]),
            progress_div
        ]));

        /// --- initial local storage loads --------------------------------------------------------------------------------
        production_input.value = localStorage.getItem("production_input") || "Turm Turn Production:Machining V2";
        measurement_input.value = localStorage.getItem("measurement_input") || "Turm Keyence Measurement:Machining V2";

        // --- add event listeners -----------------------------------------------------------------------------------------
        // --- production_input ---
        production_input.addEventListener("input", () => {
            localStorage.setItem("production_input", production_input.value);
        });

        // --- measurement_input ---
        measurement_input.addEventListener("input", () => {
            localStorage.setItem("measurement_input", measurement_input.value);
        })

        // --- Browse button ---
        browse_button.addEventListener("click", () => {
            // Reset global variables
            top_span.innerHTML = '';
            progress_div.innerHTML = '';
            all_files = [];
            productions = [];
            measurements = [];
            failed_uploads = 0;
            success_uploads = 0;
            read_files = [];
            // Trigger the click event on the file input
            file_input.click();
        });

        // --- Add a change event listener to the file input ---
        file_input.addEventListener("change", (event) => {
            all_files = event.target.files;
            let index_exists = false;
            if (all_files.length === 0) {
                browse_span.innerHTML = `<span style="color: red;">Please select all files of the folder you want to upload (Ctrl + A)</span>`;
            } else {
                for (const selected_file of all_files) {
                    if (selected_file.name === "index.txt") {
                        index_exists = true;
                        read_index(selected_file);
                        break; // Exit the loop early if index.txt is found
                    }
                }
                if (!index_exists) {
                    browse_span.innerHTML = `<span style="color: red;">Missing required file named 'index.txt'</span>`;
                }
            }
        });

        // --- Upload button ---
        upload_button.addEventListener("click", () => {
            if (productions.length + measurements.length === 0) {
                browse_span.innerHTML = `<span style="color: red;">No files to upload selected yet</span>`;
            } else {
                let progress_bar_max = 0;
                [...productions, ...measurements].forEach(({ file }) => {
                    if (file != null) {
                        progress_bar_max += 2*file.size;
                    }
                });
                progress_div.innerHTML = `
                <progress id="progress_bar" class="progress-bar" value="0" max="${progress_bar_max}"></progress>
            `;
                // --- Upload files ---
                upload_files(productions, 'true')
                upload_files(measurements, '');  // in python bool('') is False
            }
        });
    }
}

// --- HELPER FUNCTIONS ------------------------------------------------------------------------------------------------
function get_file_by_name(fileName) {
        for (const file of all_files) {
            if (file.name === fileName) {
                return file;
            }
        }
        return null; // Return null if the file with the specified name is not found
    }
    
function read_index(file) {
    const reader = new FileReader();
    reader.onload = function(e) {
        const contents = e.target.result;
        const lines = contents.split('\n');
        let branch = [];
        const productionIdentifiers = document.getElementById("production_input").value.split(':').map(elem => elem.trim());
        const measurementIdentifiers = document.getElementById("measurement_input").value.split(':').map(elem => elem.trim());

        // --- Main loop ---
        lines.forEach(line => {
            const content = line.trimStart();
            const indent = line.length - content.length;
            refreshBranch(indent, content);
            if (matchesIdentifier(productionIdentifiers)) {
                extractInstance(content, productions);
            }
            if (matchesIdentifier(measurementIdentifiers)) {
                extractInstance(content, measurements);
            }
        });
        //  --- Helper functions ---
        function extractInstance(line, appendHere) {
            const rearPart = line.split('(')[1].split(')');
            const uuid = rearPart[0];
            const instance = rearPart[1].split('-')[1];
            const file = get_file_by_name(uuid + '.xes.yaml');
            appendHere.push({ instance: instance, uuid: uuid, file: file });
        }
        function refreshBranch(indent, content) {
            branch = branch.filter(obj => obj.indent < indent);
            branch.push({indent: indent, content: content});
        }
        function matchesIdentifier(identifier) {
            if (identifier.length <= branch.length) {
                for (let i = 1; i <= identifier.length; i++) {
                    if (!branch[branch.length-i].content.includes(identifier[identifier.length-i])) {
                        return false;
                    }
                }
                return true;
            }
            return false;
        }

        // --- display results ---
        let missingFiles = 0;
        const formattedProductions = productions.map(({ instance, uuid, file }) => {
            const colorStyle = file === null ? 'color: red;' : 'color: white'; // Apply red color if the file is null
            if (file === null) {
                missingFiles += 1;
            }
            return `<span style="${colorStyle}" id="${uuid}">${instance}   ~   ${uuid}</span>`;
        });

        // Generating formatted strings for measurements with conditional style
        const formattedMeasurements = measurements.map(({ instance, uuid, file }) => {
            const colorStyle = file === null ? 'color: red;' : 'color: white'; // Apply red color if the file is null
            if (file === null) {
                missingFiles += 1;
            }
            return `<span style="${colorStyle}" id="${uuid}">${instance}   ~   ${uuid}</span>`;
        });
        const total = productions.length + measurements.length;
        const received = productions.length + measurements.length - missingFiles;
        const top_span = document.getElementById("top_span");
        const browse_span = document.getElementById("browse_span");

        if (missingFiles > 0) {
            if (received > 0) {
                browse_span.innerHTML = `
                    <span style="color: white;">Successfully selected ${received}/${total} files</span>
                    <span style="color: red;">${missingFiles} file${missingFiles > 1 ? 's are' : ' is'} missing</span>
                `;
            } else {
                browse_span.innerHTML = `<span style="color: red;">${missingFiles} file${missingFiles > 1 ? 's are' : ' is'} missing</span>`;
            }
        } else {
            browse_span.innerHTML = `<span style="color: white;">Successfully selected ${total} file${total !== 1 ? 's' : ''}</span>`;
        }
        top_span.innerHTML = `
            <div style="display: flex; justify-content: space-between;">
                <div style="flex: 1; padding-right: 10px;">
                    <h3>Productions</h3>
                    <p>${formattedProductions.join('<br>')}</p>
                </div>
                <div style="flex: 1; padding-left: 10px;">
                    <h3>Measurements</h3>
                    <p>${formattedMeasurements.join('<br>')}</p>
                </div>
            </div>
        `;
    };

    reader.readAsText(file);
}

function upload_files(file_list, is_production) {
    if (file_list.length > 0) {
        file_list.forEach(({ instance, uuid, file }) => {
            read_uuid_file(instance, uuid, is_production, file);
        });
    }

}

function read_uuid_file(instance, uuid, is_production, file) {
    const reader = new FileReader();

    reader.onload = function(e) {
        const progress_bar = document.getElementById('progress_bar');
        const yamlDocuments = jsyaml.loadAll(e.target.result);
        let datastream = {};
        try {
            yamlDocuments.forEach(document => {
                if (document.event?.['stream:datastream']) {
                    document.event['stream:datastream'].forEach(data => {
                        if (data['stream:point']) {
                            //console.log("point", data['stream:point']);
                            const streamId = data['stream:point']['stream:id'];
                            const timestamp = data['stream:point']['stream:timestamp'];
                            const value = data['stream:point']['stream:value'];
                            if (streamId !== undefined && timestamp !== undefined && value !== undefined) {
                                if (!datastream[streamId]) {
                                    datastream[streamId] = [];
                                }
                                datastream[streamId].push([timestamp, value]);
                            } else {
                                console.error("At least one of the following is missing: 'stream:id'=" + streamId + ", 'stream:timestamp'=" + timestamp + ", 'stream:value'=" + value);
                            }
                        }
                    });
                }
                progress_bar.value += file.size/yamlDocuments.length;
            });
        } catch (e) {
            console.error('Error parsing YAML:', e);
        }
        document.getElementById(uuid).style.color = 'yellow';
        // --- Upload data ---
        const formData = new FormData();
        formData.append('uuid', uuid);
        formData.append('datastream_mapping', JSON.stringify(datastream));

        // Send the data to the server
        return fetch(get_base() + 'upload_data', {
            method: 'POST',
            body: formData
        })
        .then(response => {
            progress_bar.value += file.size;
            if (!response.ok) {
                document.getElementById(uuid).style.color = 'red';
                throw new Error(`HTTP error! status: ${response.status}`);
            }
            return response.json();
        })
        .then(({ message, status_code}) => {
            document.getElementById(uuid).style.color = status_code === 200 ? 'green' : 'red';
            console.log(message + ' (' + status_code + ')');
        })
        .catch(error => {
            document.getElementById(uuid).style.color = 'red';
            console.error('Error:', error);
            // Handle failed upload, such as updating the progress bar for failure
        });
    };

    // Start the file reading process
    reader.readAsText(file);
}





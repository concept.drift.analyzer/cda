// --- IMPORTS ----------------------------------------------------------------------------------------------------------
import { create, dropdown, set_color_scheme, input } from "./basics.js";


// --- MODIFY HERE -----------------------------------------------------------------------------------------------------
// the first value in the options list is the default dropdown element
const constructor = {
    "General": {
        "Color Scheme": {options: ["System Default", "Light Mode", "Dark Mode"], func: set_color_scheme},
    },
    "Visualization": {
        "Zero Line": {options: ["Color", "On", "Off"]},
        "Discreteness": {options: ["Auto Detect", "Discrete", "Continuous"]},
        "Display Details Window": {options: ["On", "Off"], default: 'On'},
        "Max Amount Decimals": {options: "Input:0-9-:^-?(0|[1-9]\\d*)?$:Set the max amount of decimals displayed in details window.", default: ''},

        "Identification Tool": {options: ["On", "Off"], hidden: true},
        "Time Axis Format": {options: ["Auto", "Absolute", "Relative"], hidden: true},
        "Display X-Axis": {options: ["On", "Off"], hidden: true},
        "Display Y-Axis": {options: ["On", "Off"], hidden: true}
    },
    "Data Preparation": {
        "Value Filter": {options: "Input:0-9.,:^((\\d+(\\.\\d+)?)(,\\s*\\d+(\\.\\d+)?)*)?$:Insert values to remove from the datastreams.", default: '', func: "initialize_server_connection"},
        "Max Datastream Length": {options: "Input:0-9:^(0|[1-9]\\d*)?$:Define the maximum amount of data points per datastream.", default: '', func: "initialize_server_connection"},
        "Outlyer Length Deviation": {options: "Input:0-9.:^(\\d+(\\.\\d+)?)?$:Define the percentage deviation of the average length/duration.", default: ''}
    }
}


// --- MAIN FUNCTIONS --------------------------------------------------------------------------------------------------
export async function load_content(main_content, footer, title) {
    // --- variables ---
    const cb = "<div class='content_block'></div>";
    //const reset = create("<button id='reset_button'>Reset</button>");
    const settings = get_settings();

    // --- construct the body ---
    //footer.appendChild(reset);
    if (main_content) {
        for (const section_name in constructor) {
            // --- add section ---
            const section = create("<div class='container'></div>", [`<label>${section_name}</label>`], `settings_${section_name}`);
            main_content.appendChild(section);

            const dictionary = constructor[section_name];
            for (const label in dictionary) {
                // --- variables ---
                const values = dictionary[label];
                const id = label.replace(/ /g, "_").toLowerCase();
                const default_text = settings[id] !== undefined ? settings[id] : (values.default !== undefined ? values.default : values.options[0]);
                let func = values.func ?? (() => {});
                if (func === "initialize_server_connection") {
                    func = (await import("./view.mjs")).initialize_server_connection;
                }

                // --- check if hidden ---
                if (values.hidden) {
                    continue;
                }

                // --- helper function ---
                function func_dropdown(subbutton) {
                    set_settings(id, subbutton);
                    func();
                }

                function func_input(input_element) {
                    if (input_element.style.borderColor !== 'red') {
                        set_settings(id, input_element, 'value');
                        func();
                    }
                    console.log("settings = ", get_settings());
                }

                // --- create element ---
                let element;
                if (typeof values.options === "string") {
                    const seperated = values.options.split(":");
                    // as Input
                    if (seperated[0] === "Input") {
                        element = input(id, default_text, seperated[1], seperated[2], seperated[3], false, func_input);
                    } else {
                        console.error("NotImplementedError: " + seperated[0] + " is not a valid option.");
                    }
                } else {
                    // as Dropdown
                    element = dropdown(default_text, values.options, false, id, false, func_dropdown);
                }

                // --- add element ---
                section.appendChild(create(cb, [
                    "<label>" + label + ":</label>",
                    element
                ]))
            }
        }

        // --- add event listeners ---
        /*
        reset.addEventListener('click', () => {
            localStorage.setItem('settings', JSON.stringify(get_defaults()));
            remove_all_children(footer);
            remove_all_children(main_content);
            console.log("settings = ", get_settings());
            load_content(main_content, footer);
        })
        */
    }
}

// --- HELPER FUNCTIONS ------------------------------------------------------------------------------------------------
function set_settings(id, subbutton, accessor = 'textContent') {
    let settings = JSON.parse(localStorage.getItem('settings') || '{}');
    settings[id] = subbutton[accessor];
    localStorage.setItem('settings', JSON.stringify(settings));
}


export function get_settings() {
    const from_storage = JSON.parse(localStorage.getItem('settings') || '{}');
    return {...get_defaults(), ...from_storage};
}

function get_defaults() {
    // --- variables ---
    let defaults = {};

    // --- compute defaults ---
    for (const section_name in constructor) {
        const dictionary = constructor[section_name];
        for (const label in dictionary) {
            const values = dictionary[label];
            const id = label.replace(/ /g, "_").toLowerCase();
            defaults[id] = values.default !== undefined ? values.default : values.options[0];
        }
    }
    // --- cleanup ---
    return defaults;
}

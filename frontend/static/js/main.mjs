// --- IMPORTS ---------------------------------------------------------------------------------------------------------
import { load_content as load_content_upload} from './upload.mjs'
import { load_content as load_content_settings} from './settings.mjs'
import { load_content as load_content_view} from './view.mjs'
import {create, get_params, get_current_url, remove_all_children, set_color_scheme} from "./basics.js";

// --- GLOBAL VARIABLES ------------------------------------------------------------------------------------------------
const url = get_current_url();
const params = get_params();

// --- INITIALIZATION --------------------------------------------------------------------------------------------------
document.addEventListener("DOMContentLoaded", function() {
    // --- variables ---
    const body = document.body;
    const buttons = document.querySelectorAll(".navbar button");
    const plus_member = true;
    const footer = document.querySelector(".footer");
    const title = document.getElementById("title");

    // --- add event listeners ---
    buttons.forEach(button => {
        button.addEventListener("click", function() {
            // --- prepare elements ---
            remove_all_children(footer);
            remove_all_children(title);

            // --- hide previous content ---
            const divs = document.querySelectorAll("body > div");
            for (let i = 1; i < divs.length; i++) {
                if (divs[i] !== footer) {
                    divs[i].style.display = 'none';
                } else {
                    break;
                }
            }

            // --- update page param ---
            params.set("page", button.id);
            url.search = params.toString();
            window.history.replaceState({}, '', url);

            // --- show existing content, if already constructed ---
            let container = document.getElementById(`${button.id}_container`);
            const fresh_constructed = !container;

            if (button.id === "view") {
                if (!container) {
                    container = create("<div class='view' id='view_container'></div>");
                    body.insertBefore(container, footer);
                }
                load_content_view(fresh_constructed ? container : null, footer, title);
            } else {
                if (!container) {
                    const banner_container = create(`<div class='banner_container' id='${button.id}_container'></div>`);
                    body.insertBefore(banner_container, footer);

                    // --- add ad-banner if needed ---
                    if (!plus_member) {
                        const banner = document.createElement('div');
                        banner.className = 'banner';
                        banner_container.appendChild(banner);
                        const banner2 = document.createElement('div');
                        banner2.className = 'banner';
                        banner_container.appendChild(banner2);
                    }

                    // --- construct the body ---
                    container = create(`<div class='main_content' id='main_content_${button.id}'></div>`);
                    banner_container.insertBefore(container, container.lastChild);
                }

                title.appendChild(create("<h1>Concept Drift Analyzer</h1>"));
                title.className = "title";

                if (button.id === "upload") {
                    load_content_upload(fresh_constructed ? container : null, footer, title);
                } else if (button.id === "settings") {
                    load_content_settings(fresh_constructed ? container : null, footer, title);
                } else {
                    console.error("unknown button id '" + button.id + "' found in 'main.mjs' navbar");
                }
            }

            container.style.display = 'flex';
        });
    });

    // --- set default page ---
    // variables
    const validPages = ['view', 'settings', 'upload'];

    // check if page valid and set if not.
    if (!params.has('page') || !validPages.includes(params.get('page'))) {
        params.set("page", 'view');                            // here index.html is selected
        // adjust url
        url.search = params.toString();
        window.history.replaceState({}, '', url);
    }

    // --- initialize ---
    const page = params.get('page');
    document.getElementById('view').click();  // has to be loaded to reduce loading time and collect data already.
    if (page !== 'view') {
        document.getElementById(page).click();
    }
    set_color_scheme();
});

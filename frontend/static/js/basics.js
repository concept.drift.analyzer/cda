// --- imports --------------------------------------------------------------------------------------------------
import {get_settings} from "./settings.mjs";
import { get_base } from './config.js';


// --- variables -------------------------------------------------------------------------------------------------------
const dropdown_states = new Map();
const url = new URL(window.location);
const params = new URLSearchParams(url.search);

// --- main functions --------------------------------------------------------------------------------------------------
export function get_current_url() {
    return url;
}

export function get_params() {
    return params;
}

export function remove_all_children(element) {
    element.innerHTML = '';
}

export function create(parent, children = [], id = "") {
    // --- return existing, if existent ---
    if (id !== "") {
        const content = document.getElementById(id);
        if (content) {
                return content;
        }
    }

    // --- create content ---
    let constructed;
    if (typeof parent === 'string') {
        const parser = new DOMParser();
        const doc = parser.parseFromString(parent, 'text/html');
        constructed = doc.body.firstChild;
    } else {
        constructed = parent;
    }
    if (id !== "" && constructed.id === "") {
        constructed.id = id;
    }

    // --- create children ---
    children.forEach(child => {
        if (typeof child === 'string') {
            const parser = new DOMParser();
            const doc = parser.parseFromString(child, 'text/html');
            Array.from(doc.body.children).forEach(child => {
                constructed.appendChild(child);
            })
        } else {
            constructed.appendChild(child);
        }
    });

    // --- cleanup ---
    return constructed;
}

export function labeled_box(label_text, content, id = "") {
    // --- return existing, if existent ---
    if (id !== "") {
        const existing_box = document.getElementById(`${id}_box`);
        if (existing_box) {
            return existing_box;
        }
    }

    // --- create box ---
    const box = document.createElement('div');
    box.className = 'labeled_box';
    box.id = `${id}_box`;

    // --- set label ---
    const label = document.createElement('label');
    label.id = `${id}_label`;
    label.setAttribute('for', id);
    label.textContent = label_text;
    box.appendChild(label);

    // --- set content ---
    create(box, [content]);
    const content_list = Array.from(box.children);
    let already_exists= false;
    content_list.forEach(content => {
        if(content.id === `${id}`) {
            already_exists = true;
        }
    })
    content_list.forEach(content => {
        if (!content.id && !already_exists) {
            content.id = `${id}`;
            already_exists = true;
        }
        box.appendChild(content);
    })

    // --- cleanup ---
    return box;
}

export function dropdown(default_text, options, local_storage = false, id = "", multiselect = false, event_listener = (_) => {}, multiselect_count_only = true, description = "", on_selection_complete = (state) => {}, searchable = false) {
    // --- return existing, if existent ---
    if (id !== "") {
        const content = document.getElementById(`${id}_dropdown`);
        if (content) {
            return content;
        }
    }

    // --- create dropdown ---
    const dropdown = document.createElement('div');
    dropdown.className = 'dropdown';
    dropdown.id = `${id}_dropdown`;
    
    // --- create button ---
    const button = document.createElement('button');
    button.id = id;
    button.textContent = default_text;
    dropdown.appendChild(button);

    // --- create dropdown_content ---
    const dropdown_content = document.createElement('div');
    dropdown_content.className = 'dropdown_content';
    dropdown_content.id = `${id}_content`;
    dropdown.appendChild(dropdown_content);

    // --- create a state ---
    dropdown_states.set(button, {changes: new Set(), button: button, content: dropdown_content, initial_value: default_text, initial: false});

    // --- create dropdown buttons ---
    append_dropdown_options(button, dropdown_content, default_text, options, local_storage, id, multiselect, event_listener, multiselect_count_only);

    // --- resize correctly when the font size changes ---
    function resize() {
        const font_size = parseFloat(window.getComputedStyle(document.documentElement).getPropertyValue('font-size'));
        let min_width = 0;
        [...options, default_text].forEach((name) => {
            min_width = Math.max(min_width, name.length);
        });
        min_width = Math.max(min_width * font_size * 0.482 + 20, 170); // 170 is the minimum width inside content_block;; 0.482 is font-size to width factor;; 20 is additional padding
        dropdown.style.minWidth = `${min_width}px`;

        // --- resize the dropdown_content ---
        resize_dropdown_content(dropdown_content);
    }

    function update_dropdown_max_height(dropdown) {
        const dropdown_rectangle = dropdown.getBoundingClientRect();
        const marginBottomString = window.getComputedStyle(document.body).marginBottom;
        const marginBottom = parseInt(marginBottomString, 10);
        const max_dropdown_height = window.innerHeight - dropdown_rectangle.top - marginBottom;
        dropdown.style.maxHeight = max_dropdown_height + 'px';
    }

    const media_query_1 = window.matchMedia('(max-width: 1160px)');
    const media_query_2 = window.matchMedia('(max-width: 765px)');
    const media_query_3 = window.matchMedia('(max-width: 400px)');
    const media_query_4 = window.matchMedia('(max-width: 360px)');

    media_query_1.addEventListener('change', resize);
    media_query_2.addEventListener('change', resize);
    media_query_3.addEventListener('change', resize);
    media_query_4.addEventListener('change', resize);

    resize();

    // --- integrate hover description ---
    button.addEventListener('mouseenter', () => {
        dropdown_states.get(button).initial_value = button.textContent;
        dropdown_states.get(button).changes.clear();
        if (description) {
            button.textContent = description;
        }
        update_dropdown_max_height(dropdown_content);
    });

    if (description) {
        button.addEventListener('mouseleave', () => {
            button.textContent = dropdown_states.get(button).initial_value;
        });
    }

    dropdown_content.addEventListener('click', (e) => {
        if (e.target.tagName === 'A') {
            on_selection_complete(dropdown_states.get(button));
        }
    });

    // --- add searchable functionality, if needed ---
    if (searchable) {
        const input = document.createElement('input');
        input.className = 'dropdown_input';
        input.id = `${id}_input`;
        input.style.display = 'none';
        dropdown_content.before(input);

        button.addEventListener('click', () => {
            button.style.display = 'none';
            input.style.display = 'block';
            input.focus();
        });

        input.addEventListener('blur', () => {
            button.style.display = 'block';
            input.style.display = 'none';
        });

        input.addEventListener('input', (event) => {
            const searchTexts = event.target.value.toLowerCase().split(' ');
            const options = Array.from(dropdown_content.children);
            options.forEach(option => {
                const text = option.textContent.toLowerCase();
                if (searchTexts.every(searchText => text.includes(searchText))) {
                    option.style.display = '';
                } else {
                    option.style.display = 'none';
                }
            });
        });
    }

    // --- cleanup ---
    return dropdown;
}

export function append_dropdown_options(button, dropdown_content, default_text, options, local_storage = false, id = "", multiselect = false, event_listener = (_) => {}, multiselect_count_only = true, unselect = []) {
    const state = dropdown_states.get(button);
    options.forEach((option) => {
        const option_element = document.createElement('a');
        option_element.textContent = option;
        option_element.addEventListener('click', () => {

            if (multiselect) {
                // memorize changes
                if (state.changes.has(option)) {
                    state.changes.delete(option);
                } else {
                    state.changes.add(option);
                }

                // Toggle the 'selected' class on the clicked element
                option_element.classList.toggle('selected');

                // Update the dropdown button text or handle the selected items
                const selectedItems = dropdown_content.querySelectorAll('.selected');

                // Update the button text with selected items or reset if none are selected
                if (multiselect_count_only) {
                    button.textContent = `${selectedItems.length}/${options.length} Selected`;
                } else {
                    if (selectedItems.length <= 0) {
                        if (default_text === "") {
                            button.textContent = "Please Select";
                        } else {
                            button.textContent = default_text; // Reset to default text
                        }
                    } else if (selectedItems.length <= options.length) {
                        button.textContent = Array.from(selectedItems).map(item => item.textContent).join(', ');
                    }
                }
            } else {
                // memorize changes
                state.changes.clear();
                if (option !== state.initial_value) {
                    state.changes.add(option);
                }

                // update the displayed text
                button.textContent = option_element.textContent;
            }
            if (local_storage) {
                localStorage.setItem(id, button.textContent);
            }
            event_listener(option_element);
        });
        dropdown_content.appendChild(option_element);
    });

    // --- load local storage ---
    const stored_value = localStorage.getItem(id);
    const selected_values = (local_storage && stored_value) ? ((multiselect) ? stored_value.split(',') : [stored_value]) : [default_text];
    if (selected_values.includes('All Selected')) {
        state.initial = true;
        dropdown_content.querySelectorAll('a').forEach(option_element => {
            if (!unselect.includes(option_element.textContent)) {
                option_element.click();
            }
        });
        state.initial = false;
    } else {
        selected_values.forEach((value) => {
            const optionToClick = Array.from(dropdown_content.children).find(option => option.textContent === value);
            if (optionToClick) {
                optionToClick.click();
            }
        });
    }

    // --- set sub-button withs ---
    resize_dropdown_content(dropdown_content);
}




export function input(id, default_value = undefined, input_regex = undefined,
                      blur_regex = undefined, placeholder = undefined,
                      local_storage = false, event_listener = (_) => {}, type = 'text') {
    const input = document.createElement('input');
    input.placeholder = placeholder;
    input.id = id;
    input.type = type;
    input.value = local_storage ? localStorage.getItem(id) || default_value : default_value;

    // --- add input filter ---
    if (input_regex) {
        input.addEventListener('input', () => {
            input_filter(input, input_regex, blur_regex ? false : local_storage);
        });
    }

    // --- add blur filter ---
    input.addEventListener('blur', () => {
        if (blur_regex) {
            blur_filter(input, blur_regex, local_storage);
        }
        event_listener(input);
    });

    // --- cleanup ---
    return input;
}

export function input_filter(input_element, allowed_characters, add_to_local_storage) {
    const regex = new RegExp('[^' + allowed_characters + ']', 'g');
    input_element.value = input_element.value.replace(regex, '');
    if (add_to_local_storage && input_element.id) {
        localStorage.setItem(input_element.id, input_element.value);
    }
}

export function blur_filter(input_element, regex_string, add_to_local_storage) {
    const regex = new RegExp(regex_string);
    const input_value = input_element.value;
    if (!regex.test(input_value)) {
        input_element.style.borderColor = 'red';
    } else {
        if (add_to_local_storage) {
            localStorage.setItem(input_element.id, input_element.value);
        }
        input_element.style.borderColor = '';
    }
}

function resize_dropdown_content(dropdown_content) {
    const optionElements = dropdown_content.querySelectorAll('a');

    if (optionElements.length > 0) {
        const style = window.getComputedStyle(optionElements[0]);
        const fontSize = style.fontSize;
        const fontFamily = style.fontFamily;
        const font = `${fontSize} ${fontFamily}`;

        const maxWidth = Math.max(...Array.from(optionElements).map(element => {
            return get_text_width(element.textContent, font);
        }));

        const paddingAndBorderAdjustment = 32;
        const totalWidth = maxWidth + paddingAndBorderAdjustment;

        optionElements.forEach((element) => {
            element.style.minWidth = `${totalWidth}px`;
        });
    }
}

export function get_text_width(text, font) {
    const canvas = get_text_width.canvas || (get_text_width.canvas = document.createElement("canvas"));
    const context = canvas.getContext("2d");
    context.font = font;
    const metrics = context.measureText(text);
    return metrics.width;
}

/**
 * Changes the color scheme of the website based on user settings or system preferences.
 * Loads the appropriate CSS for either "Dark Mode" or "Light Mode".
 *
 * Assumes the existence of a <link> element with the id "color_scheme_link" in the HTML.
 * This function depends on `get_settings` and `get_base` functions.
 *
 * @returns {void} No return value.
 */
export function set_color_scheme() {
    const link = document.getElementById("color_scheme_link");
    const settings = get_settings();
    const dark_mode = settings['color_scheme'] === "Dark Mode" || (settings['color_scheme'] === "System Default" && window.matchMedia('(prefers-color-scheme: dark)').matches);
    link.href = dark_mode ? get_base() + "static/css/dark_mode.css" : get_base() + "static/css/light_mode.css";
}

/**
 * Creates a partial function that pre-binds some arguments.
 *
 * @param {Function} func - The original function to be partially applied.
 * @param {...any} inputs - Arguments to be bound to the original function.
 * @returns {Function} A new function that takes the remaining arguments and calls the original function.
 */
export function partial(func, ...inputs) {
    return (...more_inputs) => func(...inputs, ...more_inputs);
}

export function fill_replace_index_child(parent, child, index) {
    if (typeof parent === 'string') {
        parent = document.getElementById(parent) || create(parent);
    }
    if (typeof child === 'string') {
        child = document.getElementById(child) || create(child);
    }
    if (index === undefined || index === null) {
        remove_all_children(parent);
        parent.appendChild(child);
        return;
    }
    if (parent.children.length > index) {
        parent.children[index] = child;
    } else {
        while (parent.children.length < index) {
            parent.appendChild(document.createElement('div'));
        }
        parent.appendChild(child);
    }
}

export function get_inverted_object(obj) {
    return Object.fromEntries(Object.entries(obj).map(([key, value]) => [value, key]));
}
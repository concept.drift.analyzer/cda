/**
 * Setup: Set to the base path of the server.
 * In most cases '/'
 * With port-rerouting, e.g. '/ports/10000/'
 *
 * @returns {string} The base path of the server.
 * */
export function get_base() {
    return '/'; //'/ports/54165/'
}


/**
 * Setup: Set to the default name that is used if no stream name is given in the url.
 *
 * @returns {string} The default name that is used if no stream name is given in the url.
 */
export function get_default_name() {
    return 'measurement';
}


/**
 * Setup: Set all strings that should get detected as state instead of message.
 * Setup: The corresponding value has to be negative.
 *
 * @returns {object} The strings that should get detected as state instead of message and the corresponding value.
 */
export function get_possible_states() {
    return {
        'starting': -1,
        'running': -2,
        'stopping': -3,
        'stopped': -4,
        'error': -5,
        'active': -6,
        'Active': -6,
        'unavailable': -7,
        'Unavailable': -7,
        'Opened': -8,
        'Closed': -9,
        'Intermediate': -10,
        'Ready': -11
    };
}

/**
 * Setup: Set to an identifier that gets display that does not overlap with any other string_value identifier of any datapoint.
 *
 * @returns {string}
 */
export function get_time_id() {
    return "Datapoint_Time";
}

/**
 * Setup: Set to an identifier that gets display that does not overlap with any other string_value identifier of any datapoint.
 *
 * @returns {string}
 */
export function get_value_id() {
    return "Datapoint_Value";
}

/**
 * Setup: Set to an identifier that gets display that does not overlap with any other string_value identifier of any datapoint.
 *
 * @returns {string}
 */
export function get_message_id() {
    return "Datapoint_Message";
}

/**
 * Setup: Set to an identifier that gets display that does not overlap with any other string_value identifier of any datapoint.
 *
 * @returns {string}
 */
export function get_state_id() {
    return "Datapoint_State";
}

/**
 * Setup: Set to true, to make texts in the details window copyable, false other-vice.
 *
 * @returns {boolean}
 */
export function copyable_details() {
    return true;
}
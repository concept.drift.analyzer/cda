// --- IMPORTS ---------------------------------------------------------------------------------------------------------
import {
    remove_all_children,
    create,
    dropdown,
    partial,
    append_dropdown_options,
    get_params,
    get_current_url,
    get_inverted_object
} from './basics.js';
import {
    copyable_details,
    get_base,
    get_default_name,
    get_message_id,
    get_possible_states,
    get_state_id,
    get_time_id,
    get_value_id
} from './config.js';
import { get_settings } from "./settings.mjs";


// --- VARIABLES -------------------------------------------------------------------------------------------------------
const color_light = {background: '#FFFFFF', text: '#000000', graph: '#FFD700', graph_negative: '#6699FF', tool: '#FF0000'};
const color_dark = {background: '#000000', text: '#FFFFFF', graph: '#FFD700', graph_negative: '#6699FF', tool: '#FF0000'};
const url = get_current_url();
const params = get_params();
ensure_default_params();
let toggled_uuids = {};                 //{uuid:'on', uuid:'off', ...}
let streams = {};                       // {stream_name: [(uuid, min_timestamp) ...], ...}
set_streams();
let htmls = {};
let datastreams = {};                  // {uuid: {'time': [], 'value': []}, ...}
let sorted_datastream_keys = [];    // [uuid, ...]
let selected_sorted_datastream_keys = [];  // just the once that are selected
let str_to_number_lookup = get_possible_states();
let number_to_str_lookup = get_inverted_object(str_to_number_lookup);
const length_of_possible_states = Object.keys(str_to_number_lookup).length;
let event_source = undefined;
let discreteness = undefined;
initialize_server_connection();


// --- INITIALIZATION --------------------------------------------------------------------------------------------------
export function load_content(main_content, footer, title) {
    // --- construct the body ---
    if (main_content) {
        main_content.appendChild(create("<div class='navbar2'></div>", [
            dropdown(params.get('name'), [], false, 'name', false, (subbutton) => {url_dropdown_listener('name', subbutton);}, true, 'Stream Name', on_selection_complete, true),
            dropdown('', [], false, 'uuids', false, () => {}, true, 'UUIDs', on_selection_complete_uuid, true),
            dropdown(params.get('computation_method'), ['Duration', 'Equidistant'], false, 'visualization_method', false, (subbutton) => {
                const prev_visualization_method = params.get('computation_method');
                url_dropdown_listener('computation_method', subbutton);
                if (prev_visualization_method !== subbutton) recompute_data();},
                true, 'Mapping Method', on_selection_complete_computation_method),
            dropdown(params.get('live'), ['True', 'False'], false, 'live', false, partial(url_dropdown_listener, 'live'), true, 'Live', on_selection_complete),
        ]))
        construct_htmls();
    }
    resize_htmls();
    set_selected_sorted_datastream_keys();
    recompute_data();
}

// refresh on resize
window.addEventListener('resize', function () {
    resize_htmls();
    recompute_data();
});

// refresh if something is changed in the settings
window.addEventListener('storage', function (e) {
    if (e.key === 'settings') {
        recompute_data();
    }
});

function ensure_default_params() {
    // --- ensure default params ---
    if (!params.has('name')) params.set('name', get_default_name());
    if (!params.has('live')) params.set('live', 'True');
    if (!params.has('computation_method')) params.set('computation_method', 'Duration');

    // --- update url ---
    url.search = params.toString();
    window.history.replaceState({}, '', url);
}

function url_dropdown_listener(id, selected_subbutton) {
    params.set(id, selected_subbutton.textContent);
    url.search = params.toString();
    window.history.replaceState({}, '', url);
}

function resize_htmls() {
    const width = window.innerWidth - 16;
    const height = 0.5 * (window.innerHeight - 16 - 132);

    Array.from(Object.keys(htmls)).forEach((key) => {
        const html = htmls[key];
        if (html.parent) {
            html.parent.width = width;
            html.parent.height = height;
        }
        if (html.interactive_canvas) {
            html.interactive_canvas.width = html.set_detail && get_settings()['display_details_window'] === 'On' ? 0.7 * width : width;
            html.interactive_canvas.height = height;
        }
        if (html.background_canvas) {
            html.background_canvas.width = html.set_detail && get_settings()['display_details_window'] === 'On' ? 0.7 * width : width;
            html.background_canvas.height = height;
        }
        if (html.details_element) {
            html.details_element.width = 0.3 * width;
            html.details_element.height = height;
            html.details_element.style.display = get_settings()['display_details_window'] === 'On' ? 'flex' : 'none';
        }
    });
}

function construct_htmls() {
    // --- helper functions ---
    function create_canvas_htmls(set_detail) {
        // --- variables ---
        const details = set_detail ? create("<div class='details_element' style='flex: 1'></div>") : undefined;
        if (details !== undefined && !(copyable_details())) {
            details.style.webkitUserSelect = "none";
            details.style.mozUserSelect = "none";
            details.style.msUserSelect = "none";
            details.style.userSelect = "none";
        }
        const interactive = create("<canvas style='position: absolute; left: 8px'></canvas>");
        const background = document.createElement('canvas');
        const parent = create("<div style='flex-direction: row; display: flex'></div>", [
            background,
            interactive
        ]);

        // --- add detail, if needed ---
        if (set_detail) {
            parent.appendChild(details);
        }

        // --- cleanup ---
        return {
            parent: parent,
            background_canvas: background,
            interactive_canvas: interactive,
            details_element: details,
            set_detail: set_detail
        };
    }

    // --- variables ---
    const top = create_canvas_htmls(true);
    const bot = create_canvas_htmls(false);
    bot.details_element = top.details_element;
    htmls = {itvs_top: top, itvs_bot: bot};

    // --- resize ---
    resize_htmls();

    // --- construct the body ---
    const view_container = document.getElementById('view_container');
    while (view_container.children.length > 1) {
        view_container.removeChild(view_container.lastElementChild);
    }
    const keys = Object.keys(htmls);
    for (let i = 0; i < keys.length; i++) {
        view_container.appendChild(htmls[keys[i]].parent);
    }
}

function set_streams() {
    streams = {};
    fetch(get_base() + 'get_streams')
        .then(response => {
            // Check if the response is successful
            if (!response.ok) {
                throw new Error('Error with Network response: ' + response);
            }
            return response.json();
        })
        .then(json => {
            // Handle the c from the response
            if (json.status_code === 200) {
                streams = json.data;

                // Add each stream key as a dropdown item
                set_name_content(Object.keys(streams).sort((a, b) => a.length - b.length));


            } else {
                handle_error('Error fetching streams:' + json.message + ' (' + json.status_code + ')');
            }
        })
        .catch(error => {
            // Handle any other errors that occur during the fetch process
            handle_error('Fetch error:', error.message);
        });
}

export function initialize_server_connection() {
    // --- PARSE URL ARGUMENTS ---
    const name = params.get('name');
    const live = params.get('live') === 'True';
    const value_filter = get_settings()['value_filter'];
    const max_length = get_settings()['max_datastream_length'];
    const method = live ? 'listen' : 'get_datastreams';
    const url = `${get_base()}${method}?stream_name=${name}&value_filter=${value_filter}&max_length=${max_length === '' ? 0 : max_length}`;

    // --- initialize connection ---
    if (event_source !== undefined) {
        event_source.close();
    }
    if (live) {
        // --- CREATE SSE CONNECTION ---
        event_source = new EventSource(url);
        // handle event
        event_source.onmessage = function(event) {
            const data = JSON.parse(event.data);
            update_live_data(data);
        };
        // handle error
        event_source.onerror = function(error) {
            handle_error("EventSource error:", error);
            event_source.close();
        };
    } else {
        // --- FETCH DATA ONCE ---
        fetch(url)
        .then(response => {
            if (!response.ok) {
                handle_error("HTTP error: " + response.status + " " + response.statusText);
            }
            return response.json();
        })
        .then(({ message, status_code}) => {
            if (status_code !== 200) {
                handle_error(message + ' (' + status_code + ')');
            } else {  // message here is the data dict.
                console.time("new_data_incoming_offline")
                update_data(message);
                console.timeEnd("new_data_incoming_offline");
            }
        })
        .catch(error => {
            handle_error("Fetch error:", error);
        });
    }
}


// --- MAIN FUNCTIONS --------------------------------------------------------------------------------------------------
function update_live_data(data_dict) {
    if (data_dict['type'] === 'new_datastreams') {
        const new_streams = data_dict['data'];
        const uuid = data_dict['uuid'];
        for (const [stream_name, min_timestamp] of Object.entries(new_streams)) {
            const stream_name_got_added = !(stream_name in streams);
            if (stream_name_got_added) {
                streams[stream_name] = [];
            }

            // add uuid to stream at the correct position
            const min_timestamps = streams[stream_name].map(tuple => tuple[1]);
            const insertBeforeIndex = min_timestamps.findIndex((timestamp) => timestamp > min_timestamp);

            const content = document.getElementById('uuids_content');

            // add the uuid to the dropdown at the correct position
            set_uuids_content([uuid]);
            if (insertBeforeIndex !== -1) {
                streams[stream_name].splice(insertBeforeIndex, 0, [uuid, min_timestamp]);
                content.insertBefore(content.lastElementChild, content.children[insertBeforeIndex]);
            } else {
                streams[stream_name].push([uuid, min_timestamp]);
            }

            // update name dropdown, if needed
            if (stream_name_got_added) {
                // variables
                const content = document.getElementById('name_content');
                const length = stream_name.length;
                const lengths = Array.from(content.children).map(child => child.textContent.length);
                const insertBeforeIndex = lengths.findIndex((childLength) => childLength > length);

                // add the element to the dropdown at the correct position
                set_name_content([stream_name]);
                if (insertBeforeIndex !== -1) {
                    content.insertBefore(content.lastElementChild, content.children[insertBeforeIndex]);
                }
            }

            // refresh the canvas if needed
            if (stream_name === params.get('name')) {
                set_selected_sorted_datastream_keys();
                recompute_data();
            }
        }
    } else {
        update_data(data_dict);
    }
}


function update_data(data_dict) {

    const data = data_dict['data'];
    if (data_dict['type'] === 'initial') {
        str_to_number_lookup = get_possible_states();
        number_to_str_lookup = get_inverted_object(str_to_number_lookup);
        discreteness = undefined;
    }
    set_discreteness(data_dict['discreteness']);

    // --- Handle Initial Data ---
    if (data_dict['type'] === 'initial' && discreteness <= 1) {
        sorted_datastream_keys = data_dict['sorted_uuids'];
        datastreams = data;
        set_selected_sorted_datastream_keys();
        recompute_data();
        return;
    }

    // --- UPDATE SORTED_DATASTREAM_KEYS ---
    if (data_dict['type'] === 'initial') {
        sorted_datastream_keys = data_dict['sorted_uuids'];
        sorted_datastream_keys = data_dict['sorted_uuids'];
        datastreams = {};
        for (const key of sorted_datastream_keys) {
            datastreams[key] = {'time': [], 'value': []};
        }
    } else {
        const amount_uuids = sorted_datastream_keys.length;
        for (const [key, dictionary] of Object.entries(data)) {
            if (!(key in sorted_datastream_keys)) {
                const first_timestamp = dictionary['time'][0];
                let index = sorted_datastream_keys.length - 1;
                for (; index >= 0; index--) {
                    if(datastreams[sorted_datastream_keys[index]]['time'][0] <= first_timestamp) {
                        if (index === sorted_datastream_keys.length - 1) {
                            let max_datastream_length = get_settings()['max_datastream_length'];
                            max_datastream_length = max_datastream_length === '' ? '0' : max_datastream_length;
                            max_datastream_length = parseInt(max_datastream_length);
                            if (max_datastream_length !== 0) {
                                const current_newest_uuid = sorted_datastream_keys[sorted_datastream_keys.length - 1];
                                datastreams[current_newest_uuid].time = interpolate(datastreams[current_newest_uuid].time, max_datastream_length)
                                datastreams[current_newest_uuid].value = interpolate(datastreams[current_newest_uuid].value, max_datastream_length)
                            }
                            sorted_datastream_keys.push(key);
                        } else {
                            sorted_datastream_keys.splice(index + 1, 0, key);
                        }
                        datastreams[key] = {'time': [], 'value': []};
                        break;
                    }
                }
                if (index === -1) {
                    sorted_datastream_keys.unshift(key);
                    datastreams[key] = {'time': [], 'value': []};
                }
            }
        }
    }

    // --- UPDATE DATASTREAMS ---
    let repaint = false;
    for (const [key, dictionary] of Object.entries(data)) {
        if (!dictionary.hasOwnProperty('value') || !dictionary.hasOwnProperty('time') || dictionary.value.length !== dictionary.time.length) {
            console.error("Invalid dictionary:", dictionary);
            return;
        }
        if (dictionary['value'].length === 0) {
            continue;
        }

        dictionary['value'].forEach((value, index, array) => {
            if (typeof value === 'string') {
                if (!(value in str_to_number_lookup)) {
                    str_to_number_lookup[value] = Object.keys(str_to_number_lookup).length - length_of_possible_states;
                    number_to_str_lookup[str_to_number_lookup[value]] = value;
                }
                array[index] = str_to_number_lookup[value];
            }
        });

        if (datastreams[key]['time'].length > 0) {
            const last_index = datastreams[key]['time'].length - 1;
            let current_index_new = 0;
            while (data[key]['time'][current_index_new] <= datastreams[key]['time'][last_index]) {
                if (data[key]['time'][current_index_new] === datastreams[key]['time'][last_index]) {
                    if (data[key]['value'][current_index_new] === datastreams[key]['value'][last_index]) {
                        break;
                    } else {
                        current_index_new += 1;
                    }
                } else {
                    current_index_new += 1;
                }
            }
            datastreams[key]['time'].push(...data[key]['time'].slice(current_index_new));
            datastreams[key]['value'].push(...data[key]['value'].slice(current_index_new));
            if (datastreams[key]['time'].length > last_index + 1) {
                repaint = true;
            }
        } else {
            datastreams[key]['time'] = data[key]['time'];
            datastreams[key]['value'] = data[key]['value'];
        }


    }

    set_selected_sorted_datastream_keys();

    recompute_data();
}


function recompute_data() {
    // --- check for availability of enough data ---
    if (selected_sorted_datastream_keys.length <= 1) {  // if you change to <= 0 here, you divide by zero in computation of average_length
        Object.entries(htmls).forEach(([key, value]) => {
            get_canvas({html: value, datastream: {no_data: true}});
        });
        return;
    }

    // --- variables ---
    const newest_key = selected_sorted_datastream_keys[selected_sorted_datastream_keys.length - 1];
    let newest_datastream = datastreams[newest_key];
    if (newest_datastream === undefined) {
        return;
    }
    const computation_method = params.get('computation_method');
    const live = params.get('live') === 'True';
    let data = {html: {}, details: [], canvas: {}, settings: get_settings(), datastream: {value: [], time: []}, interactive: {}};

    // --- visualization mode ---
    // variables
    const newest_data = datastreams[newest_key] || { 'value': [], 'time': [] };
    const second_newest_data = datastreams[selected_sorted_datastream_keys[selected_sorted_datastream_keys.length - 2]] || { 'value': [], 'time': [] };

    // set special data
    data.canvas.padding = {right: data.settings['time_axis_format'] === 'Auto' && data.settings['display_details_window'] !== 'Auto' ? 40 : undefined};
    data.datastream.time = newest_data['time'].slice();
    data.interactive.on_initial_details = true;
    data.interactive.on_initial_func = true;
    data.html = htmls.itvs_bot;

    // average duration
    const reduced_selected_sorted_datastream_keys = selected_sorted_datastream_keys.filter((value) => value !== sorted_datastream_keys[sorted_datastream_keys.length - 1]);
    let average_duration = undefined;
    const own_duration = computation_method === 'Duration' ? newest_data['time'][newest_data['time'].length - 1] - newest_data['time'][0] : newest_data['time'].length;
    if (live && reduced_selected_sorted_datastream_keys.length !== selected_sorted_datastream_keys.length) {
        average_duration = get_average_duration(reduced_selected_sorted_datastream_keys);
        average_duration = Math.max(average_duration, own_duration);
    }

    data.interactive.func = (exploded_data) => {
        if ( exploded_data.on_padding ) {
            return;
        }
        let new_data = {html: htmls.itvs_top, details: [], canvas: {shift_value_label_not_on_initial: -6}, settings: get_settings(),
            datastream: {value: [], time: []}, interactive: {on_initial_tool_time: false, on_initial_tool_value: false, initial_call: (exploded_data.interactive ?? {}).initial_call}};
        new_data.settings['time_axis_format'] = new_data.settings['time_axis_format'] === 'Auto' ? 'Absolute' : new_data.settings['time_axis_format'];
        new_data.settings['discreteness'] = 'Discrete';
        selected_sorted_datastream_keys.forEach((key, index) => {
            if (params.get('live') === 'True' && index === selected_sorted_datastream_keys.length - 1 && average_duration > own_duration) {
                new_data.datastream.value.push(exploded_data.datastream.value[exploded_data.interactive.event.current_index]);
                new_data.datastream.time.push(exploded_data.datastream.time[exploded_data.interactive.event.current_index]);
            } else {
                const equivalent_index = get_index_from_index(exploded_data.datastream.time, exploded_data.interactive.event.current_index, datastreams[key]['time'], undefined, data.settings['discreteness']);
                new_data.datastream.value.push(datastreams[key]['value'][equivalent_index]);
                new_data.datastream.time.push(datastreams[key]['time'][equivalent_index]);
            }
        });
        new_data.details = get_details(new_data, new_data.datastream.value)
        get_canvas(new_data);
    }

    // set data.datastream.value/time
    if (selected_sorted_datastream_keys.length >= 2) {
        for(let i = 0; i < newest_data['value'].length; i++) {
            const second_index = get_index_from_index(newest_data['time'], i, second_newest_data['time'], average_duration, data.settings['discreteness']);
            data.datastream.value.push(newest_data['value'][i] - second_newest_data['value'][second_index]);
        }
        if (average_duration !== undefined && average_duration !== own_duration) {
            if (computation_method === 'Duration') {
                if (average_duration > own_duration + 0.001) {
                    data.datastream.value.push(undefined);
                    data.datastream.time.push(newest_data['time'][newest_data['time'].length - 1] + 0.001);
                }
                data.datastream.value.push(undefined);
                data.datastream.time.push(newest_data['time'][0] + average_duration);
            } else {  // Equidistant
                data.datastream.value = expand_array(data.datastream.value, average_duration);
                const max_value = data.datastream.time[0] + get_average_duration(reduced_selected_sorted_datastream_keys, 'Duration');
                data.datastream.time = expand_array(data.datastream.time, average_duration, max_value);
            }
        }
    }
    data.details = get_details(data, newest_datastream.value);
    get_canvas(data);
}



function get_canvas(data) {
    const value_label_shift = (data.interactive ?? {}).initial_call === false && (data.canvas ?? {}).shift_value_label_not_on_initial !== undefined ? (data.canvas ?? {}).shift_value_label_not_on_initial : 0;
    // --- helper functions ---
    const explode_data = (data) => {
        // --- initial sets ---
        data.html = data.html ?? {};
        data.canvas = data.canvas ?? {};
        data.settings = data.settings ?? {};
        data.datastream = data.datastream ?? {};
        data.interactive = data.interactive ?? {};
        data.settings = {...get_settings(), ...data.settings};
        data.interactive.interaction_needed = data.settings['identification_tool'] !== 'Off' ||
            data.settings['display_details_window'] !== 'Off' ||
            data.interactive.func !== undefined;
        data.interactive.is_clicked = false;

        // --- check html elements ---
        // background_canvas
        if (!data.html.background_canvas) {
            handle_error('data.html.background_canvas is not defined');
        }
        if (!document.body.contains(data.html.background_canvas)) {
            handle_error('data.html.background_canvas is not a (grand-)child of the html-document');
        }

        if (data.interactive.interaction_needed) {
            // interactive_canvas
            if (!data.html.interactive_canvas) {
                handle_error('data.html.interactive_canvas is not defined');
            }
            if (!document.body.contains(data.html.interactive_canvas)) {
                handle_error('data.html.interactive_canvas is not a (grand-)child of the html-document');
            }
            if (data.html.interactive_canvas.width !== data.html.background_canvas.width) {
                handle_error('data.html.interactive_canvas and data.html.background_canvas must have the same width');
            }
            if (data.html.interactive_canvas.height !== data.html.background_canvas.height) {
                handle_error('data.html.interactive_canvas and data.html.background_canvas must have the same height');
            }

            // details_element
            if (data.settings['display_details_window'] !== 'Off') {
                if (!data.html.details_element) {
                    handle_error('data.html.details_element is not defined');
                }
                if (!document.body.contains(data.html.details_element)) {
                    handle_error('data.html.details_element is not a (grand-)child of the html-document');
                }
            }
        }

        // --- color ---
        const color = (data.settings['color_scheme'] === "Dark Mode" ||
            (data.settings['color_scheme'] === "System Default" && window.matchMedia('(prefers-color-scheme: dark)').matches)) ?
            color_dark : color_light;
        data.canvas.color = data.canvas.color ?? {};
        data.canvas.color = {...color, ...data.canvas.color};
        if (data.html.details_element) {
            data.html.details_element.style.backgroundColor = data.canvas.color.background;
            data.html.details_element.style.color = data.canvas.color.text;
        }

        // --- check availability of data ---
        if (data.datastream.no_data) {
            data.canvas.context = data.canvas.context ?? data.html.background_canvas.getContext('2d');
            return data;
        }

        // --- datastream ---
        data.datastream.time = timestamps_to_seconds(data.datastream.time) ?? [];  // todo is that really needed. i am sure it already has that format!
        data.datastream.value = data.datastream.value ?? [];
        if (data.datastream.time.length !== data.datastream.value.length) {
            throw new Error("Length of value and time arrays do not match. Drawing a canvas is impossible.");
        }
        if (data.datastream.time.length === 0) {
            data.datastream.no_data = true;
        } else {
            data.datastream.no_data = false;
        }
        data.datastream.relative_time = data.datastream.relative_time ?? data.datastream.time.map(t => t - data.datastream.time[0]);
        data.datastream.datetime = data.datastream.datetime ?? data.datastream.time.map(t => new Date(t * 1000));
        data.datastream.no_number_data = data.datastream.no_number_data ?? false;
        if (data.datastream.value.length !== data.datastream.time.length) {
            handle_error("Length of value and time arrays do not match. Drawing a canvas is impossible.");
            data.datastream.no_data = true;
        }
        const numbers = data.datastream.value.filter(item => typeof item === 'number');
        if (numbers.length === 0) {
            data.datastream.no_number_data = true;
            data.datastream.min_value = data.datastream.min_value ?? undefined;
            data.datastream.max_value = data.datastream.max_value ?? undefined;
        } else {
            data.datastream.max_value = data.datastream.max_value ?? Math.max(...numbers);
            data.datastream.min_value = data.datastream.min_value ?? Math.min(...numbers);
        }
        data.datastream.time_for_label = data.datastream.time_for_label ?? (data.settings['time_axis_format'] === 'Absolute' ? data.datastream.datetime : data.datastream.relative_time);

        // --- canvas ---
        // general
        data.canvas.font = 'normal 12px Arial'; // data.canvas.font ?? '12px Arial';
        data.canvas.padding = data.canvas.padding ?? {};
        data.canvas.padding.top = data.canvas.padding.top ?? 20;
        data.canvas.padding.right = data.canvas.padding.right ?? (data.settings['time_axis_format'] === 'Absolute' ? 40 : 20);
        data.canvas.padding.left = data.canvas.padding.left ?? (data.settings['display_y-axis'] !== 'Off' ? 66 : 20);  // 37.34765625px for font "normal 12px Arial" as max value label width. *10 for "normal 120px Arial" // todo if a value is greater than 10(26) the label might be bigger
        data.canvas.padding.bottom = data.canvas.padding.bottom ?? (data.settings['display_x-axis'] !== 'Off' ? (data.datastream.datetime ? 50 : 40) : 20);
        data.canvas.inner_width = data.canvas.inner_width ?? (data.html.background_canvas.width - data.canvas.padding.left - data.canvas.padding.right);
        data.canvas.inner_height = data.canvas.inner_height ?? (data.html.background_canvas.height - data.canvas.padding.top - data.canvas.padding.bottom);
        data.canvas.context = data.canvas.context ?? data.html.background_canvas.getContext('2d');
        data.canvas.max_time_label_width = get_max_label_width(data);

        // --- interactive ---
        if (data.interactive.interaction_needed) {
            data.interactive.context = data.interactive.context ?? data.html.interactive_canvas.getContext('2d');
            data.interactive.on_initial_func = data.interactive.on_initial_func ?? true;
            data.interactive.on_initial_details = data.interactive.on_initial_details ?? false;
            data.interactive.on_initial_tool_time = data.interactive.on_initial_tool_time ?? true;
            data.interactive.on_initial_tool_value = data.interactive.on_initial_tool_value ?? true;
            data.interactive.event = data.interactive.event ?? {};
            data.interactive.event.current_index = data.interactive.event.current_index ?? data.datastream.time.length - 1;
            data.interactive.initial_call = false;

            // add event listener
            const setupCanvas = (canvas, data) => {
                if (canvas === undefined) {
                    return;
                }
                if (canvas.removeEventListeners !== undefined) {
                    canvas.removeEventListeners();
                }
                function handleMouseDown(event) {
                    console.time("clicked");
                    data.interactive.is_clicked = true;
                    interactive_canvas_event_func(data, event);
                    console.timeEnd("clicked");
                }

                function handleMouseMove(event) {
                    if (data.interactive.is_clicked) {
                        interactive_canvas_event_func(data, event);
                    }
                }

                function handleMouseUp(event) {
                    data.interactive.is_clicked = false;
                    interactive_canvas_event_func(data, event, false);
                }

                function handleMouseLeave() {
                    data.interactive.is_clicked = false;
                }

                // function to remove event listeners
                function removeAllEventListeners() {
                    canvas.removeEventListener('mousedown', handleMouseDown);
                    canvas.removeEventListener('mousemove', handleMouseMove);
                    canvas.removeEventListener('mouseup', handleMouseUp);
                    canvas.removeEventListener('mouseleave', handleMouseLeave);

                    // also clear the canvas
                    if(data.interactive.context) {
                        data.interactive.context.clearRect(0, 0, data.html.interactive_canvas.width, data.html.interactive_canvas.height);
                    }
                }
                canvas.removeEventListeners = removeAllEventListeners;

                // add new event listeners
                canvas.addEventListener('mousedown', handleMouseDown);
                canvas.addEventListener('mousemove', handleMouseMove);
                canvas.addEventListener('mouseup', handleMouseUp);
                canvas.addEventListener('mouseleave', handleMouseLeave);
            }
            setupCanvas(data.html.interactive_canvas, data);
        }

        // --- cleanup ---
        return data;
    }
    const scaleY = (d) => {
        if (data.datastream.min_value === data.datastream.max_value) {
            return data.canvas.padding.top + data.canvas.inner_height / 2;
        } else {
            return (data.canvas.inner_height + data.canvas.padding.top) - ((d - data.datastream.min_value) / (data.datastream.max_value - data.datastream.min_value) * data.canvas.inner_height);
        }
    }
    const scaleX = (i) => {
        return data.canvas.padding.left + (get_factor_from_index(data.datastream.time, i) * data.canvas.inner_width);
    }
    // --- variables ---
    data = explode_data(data);

    // --- Set background color ---
    data.canvas.context.fillStyle = data.canvas.color.background;
    data.canvas.context.fillRect(0, 0, data.html.background_canvas.width, data.html.background_canvas.height);

    // --- check if no data ---
    if (data.datastream.no_data) {
        // measure text size
        const maxWidth = data.html.background_canvas.width * 0.75;
        const maxHeight = data.html.background_canvas.height * 0.75;
        let fontSize = Math.min(data.html.background_canvas.width / 5, data.html.background_canvas.height / 4);
        data.canvas.context.font = `${fontSize}px Arial`;
        let textMetrics = data.canvas.context.measureText('NO DATA');

        // adjust font size
        const scale = Math.min(maxWidth / textMetrics.width, maxHeight / textMetrics.height);
        fontSize = fontSize * scale;
        data.canvas.context.font = `${fontSize}px Arial`;

        // text settings
        data.canvas.context.fillStyle = data.canvas.color.text;
        data.canvas.context.textAlign = 'center';
        data.canvas.context.textBaseline = 'middle';

        // draw text
        data.canvas.context.fillText(
            'NO DATA',
            data.html.background_canvas.width / 2,
            data.html.background_canvas.height / 2
        );

        // remove interactive canvas if it exists
        if (data.html.interactive_canvas !== undefined && data.html.interactive_canvas.removeEventListeners !== undefined) {
            data.html.interactive_canvas.removeEventListeners();
        }

        return data;
    }

    // --- draw zero line ---
    const y_zero = scaleY(0);
    if (data.settings['zero_line'] !== 'Off' && data.datastream.min_value <= 0 && 0 <= data.datastream.max_value) {
        data.canvas.context.beginPath();
        data.canvas.context.strokeStyle = data.canvas.color.text;
        data.canvas.context.lineWidth = 1;

        data.canvas.context.moveTo(data.canvas.padding.left, y_zero);
        data.canvas.context.lineTo(data.canvas.inner_width + data.canvas.padding.left, y_zero);

        data.canvas.context.stroke();
        data.canvas.context.closePath();
    }

    // --- draw y-axis ---
    if (data.settings['display_y-axis'] !== 'Off') {
        data.canvas.context.beginPath();
        data.canvas.context.strokeStyle = data.canvas.color.text;
        data.canvas.context.lineWidth = 1;
        data.canvas.context.moveTo(data.canvas.padding.left, data.canvas.padding.top);
        data.canvas.context.lineTo(data.canvas.padding.left, data.canvas.inner_height + data.canvas.padding.top);

        // Draw ticks and labels
        const font_size_match = data.canvas.font.match(/\d+/);
        const font_size = font_size_match ? parseInt(font_size_match[0], 10) : 12; // 12 is the default value if no match is found
        const available_space = data.canvas.inner_height;
        let amount_labels = Math.floor(available_space / (font_size * 3));

        for (let i = 0; i <= amount_labels; i++) {
            const y = (data.canvas.padding.top + (i / amount_labels) * data.canvas.inner_height);
            const val = data.datastream.min_value + (1 - i / amount_labels) * (data.datastream.max_value - data.datastream.min_value);

            // Draw tick
            data.canvas.context.moveTo(data.canvas.padding.left - 5, y);
            data.canvas.context.lineTo(data.canvas.padding.left, y);

            // Draw label
            data.canvas.context.font = data.canvas.font;
            data.canvas.context.textAlign = 'right';
            data.canvas.context.fillStyle = data.canvas.color.text;
            data.canvas.context.fillText(value_to_label(val), data.canvas.padding.left - 8, y + 3 + value_label_shift);
        }
        data.canvas.context.stroke();
    }

    // --- draw x-axis ---
    if (data.settings['display_x-axis'] !== 'Off') {
        data.canvas.context.beginPath();
        data.canvas.context.strokeStyle = data.canvas.color.text;
        data.canvas.context.lineWidth = 1;

        // draw axis itself
        data.canvas.context.moveTo(data.canvas.padding.left, data.canvas.inner_height + data.canvas.padding.top);
        data.canvas.context.lineTo(data.canvas.inner_width + data.canvas.padding.left, data.canvas.inner_height + data.canvas.padding.top);
        data.canvas.context.stroke();

        // compute label amount
        const max_label_width = data.canvas.max_time_label_width  * (data.settings['datetime_format'] === 'Absolute' ? 0.7 : 2);  // 0.7 and 2 are just for nice looking spacing
        let max_label_count = Math.floor(data.canvas.inner_width / max_label_width);
        if (max_label_count > data.datastream.time.length) {
            max_label_count = data.datastream.time.length;
        }
        if (max_label_count < 2) {
            max_label_count = 2;
        }
        const label_interval = Math.ceil((data.datastream.time_for_label.length / max_label_count));

        // function to draw x-axis labels
        const draw_x_label = (x, time) => {
            if (data.settings['time_axis_format'] === 'Absolute') {
                const [date_part, time_part] = timestamp_to_label(time, data);

                // draw date
                data.canvas.context.fillStyle = data.canvas.color.text;
                data.canvas.context.textAlign = 'center';
                data.canvas.context.textBaseline = 'bottom';
                data.canvas.context.fillText(date_part, x, data.canvas.inner_height + data.canvas.padding.top + 30);

                // draw time
                data.canvas.context.textBaseline = 'top';
                data.canvas.context.fillText(time_part, x, data.canvas.inner_height + data.canvas.padding.top + 30);
            } else {
                const label = timestamp_to_label(time, data);
                data.canvas.context.save();
                data.canvas.context.translate(x, data.canvas.inner_height + data.canvas.padding.top + 20);
                data.canvas.context.textAlign = 'center';
                data.canvas.context.fillStyle = data.canvas.color.text;
                data.canvas.context.fillText(label, 0, 0);
                data.canvas.context.restore();
            }

            // small line per label
            data.canvas.context.beginPath();
            data.canvas.context.moveTo(x, data.canvas.inner_height + data.canvas.padding.top);
            data.canvas.context.lineTo(x, data.canvas.inner_height + data.canvas.padding.top + 5);
            data.canvas.context.stroke();
        }

        // compute and draw the labels
        if (max_label_count > 0) {

            if (params.get('computation_method') === 'Duration') {
                for (let i = 0; i < max_label_count; i++) {
                    let position;
                    if (max_label_count === 1) {
                        position = 1;
                    } else {
                        position = i / (max_label_count - 1);
                    }
                    const time = position * (data.datastream.time[data.datastream.time.length - 1] - data.datastream.time[0]) + data.datastream.time[0];
                    const x = data.canvas.padding.left + data.canvas.inner_width * position;
                    draw_x_label(x, time);
                }
            } else {
                const evenInterval = data.canvas.inner_width / (max_label_count - 1);
                for (let i = 0; i < max_label_count; i++) {
                    const timeIndex = Math.round(i * (data.datastream.time.length - 1) / (max_label_count - 1));
                    const time = data.datastream.time[timeIndex];
                    const x = data.canvas.padding.left + evenInterval * i;
                    draw_x_label(x, time);
                }
            }
        }
    }

    // --- draw graph ---
if (data.datastream.value.length > 0) {
        data.canvas.context.strokeStyle = data.settings['zero_line'] !== 'Color' || data.datastream.value[0] >= 0 ? data.canvas.color.graph : data.canvas.color.graph_negative;
        data.canvas.context.lineWidth = 3;
        data.canvas.context.beginPath();
        let prev_x;
        let prev_y;
        const move_to = (x, y) => {
            if (data.settings['zero_line'] === 'Color') {
                prev_x = x;
                prev_y = y;
            }
            data.canvas.context.moveTo(x, y);
        }
        const line_to = (x, y) => {
            if (data.settings['zero_line'] !== 'Color') {
                data.canvas.context.lineTo(x, y);
            } else {
                if (prev_y <= y_zero && y <= y_zero) {
                    // both positive
                    data.canvas.context.lineTo(x, y);
                } else if (prev_y > y_zero && y > y_zero) {
                    // both negative
                    data.canvas.context.lineTo(x, y);
                } else {
                    const intersection_x = x - (x - prev_x) * (y - y_zero) / (y - prev_y);
                    if (prev_y > y_zero && y <= y_zero) {
                        // negative to positive
                        data.canvas.context.lineTo(intersection_x, y_zero);
                        data.canvas.context.stroke();
                        data.canvas.context.beginPath();
                        data.canvas.context.strokeStyle = data.canvas.color.graph;
                        data.canvas.context.lineWidth = 3;
                        move_to(intersection_x, y_zero);
                        data.canvas.context.lineTo(x, y);
                    } else {
                        // positive to negative
                        data.canvas.context.lineTo(intersection_x, y_zero);
                        data.canvas.context.stroke();
                        data.canvas.context.beginPath();
                        data.canvas.context.strokeStyle = data.canvas.color.graph_negative;
                        data.canvas.context.lineWidth = 3;
                        move_to(intersection_x, y_zero);
                        data.canvas.context.lineTo(x, y);
                    }
                }
                prev_x = x;
                prev_y = y;
            }
        }
        move_to(scaleX(0), scaleY(data.datastream.value[0]));
        const used_discreteness = get_discreteness(data.settings['discreteness']);
        data.datastream.value = data.datastream.value.map(value => value === null ? undefined: value);  // todo es sollte direkt undefined gesetzt werden
        for (let i = 0; i < data.datastream.value.length - 1; i++) {
            // variables
            const value = data.datastream.value[i];
            const x = scaleX(i);
            const y = scaleY(value);
            const next_value = i === data.datastream.value.length - 1 ? undefined : data.datastream.value[i + 1];
            const next_x = scaleX(i + 1);
            const next_y = scaleY(next_value);
            // plotting
            if (value === null || value === undefined) {
                move_to(next_x, next_y);
            } else {
                if (used_discreteness !== 0) {
                    line_to(next_x, y);
                    line_to(next_x, next_y);
                } else {
                    if (next_value === null || next_value === undefined) {
                        // this just draws a dot
                        line_to(x, y);
                        move_to(next_x, next_y);
                    } else {
                        line_to(next_x, next_y);
                    }
                }
            }
        }

        // draw last line
        if (data.datastream.value[data.datastream.value.length - 1] !== null && data.datastream.value[data.datastream.value.length - 1] !== undefined) {
            // this just draws a dot
            line_to(data.canvas.inner_width + data.canvas.padding.left, scaleY(data.datastream.value[data.datastream.value.length - 1]));
        }
        data.canvas.context.stroke();
    }
    

    // --- initial call ---
    let newest_index = null;
    for (let i = data.datastream.time.length - 1; i >= 0; i--) {
        if (data.datastream.value[i] !== null && data.datastream.value[i] !== undefined) {
            newest_index = i;
            break;
        }
    }


    const ClientX = newest_index !== null ? scaleX(newest_index) : data.canvas.padding.left;
    const clientY = data.canvas.padding.top + data.canvas.inner_height * 0.5;
    interactive_canvas_event_func({...data, interactive: {...data.interactive, initial_call: true}},
        {on_right_padding: false, on_left_padding: false, on_top_padding: false, on_bottom_padding: false, index_factor: 1, value_factor: 0.5},
        false);

    // --- cleanup ---
    return data;
}


// --- HELPER FUNCTIONS ------------------------------------------------------------------------------------------------
function get_discreteness(settings_discreteness = 'Auto Detect') {
    return ((settings_discreteness === 'Auto Detect' || settings_discreteness === undefined) ?
        discreteness : (settings_discreteness === 'Discrete' ? 1 : 0));
}



// --- recompute_data helper ---
function get_detail(time, value, time_axis_format, string_value = undefined) {
    // todo this is not possible incremental, because i would have to memorize original_values and it is not possible because of previous_state (falls das so sein soll) 
    // todo i think this is better than saving all details in memory.
    //  also it is not necessary to compute details for the main visualization, because detail is always used for the trend graph. -> no value is different !!
    //  because when difference graph is clicked, it is equivalent to the newest element in trend_graph, besides value!!!!!
}

function get_details(data, original_values) {
    const details = [];
    const times = data.datastream.time;
    const values = data.datastream.value;
    const max_amount_decimals = data.settings['max_amount_decimals'] !== undefined && data.settings['max_amount_decimals'] !== '' ? parseInt(data.settings['max_amount_decimals']) : -1;
    for (let i = 0; i < times.length; i++) {
        details.push({});
        let value = values[i]
        const time = times[i];

        // set time
        if ( data.settings['time_axis_format'] === 'Absolute' ) {
            details[i][get_time_id()] = timestamp_to_label(time, data, true);
        } else {
            details[i][get_time_id()] = timestamp_to_label(time, data, true);
        }

        // set value
        if (value === null || value === undefined) {
            value = 'N/A';
        } else if (max_amount_decimals < 0) {
             value = value.toString();
        } else {
            value = value.toFixed(max_amount_decimals);
        }
        details[i][get_value_id()] = value.replace(/\.0+$|(\.\d*?[1-9])0+$/, '$1');

        // set state, message and custom string values
        if (i < original_values.length && original_values[i] in number_to_str_lookup) {
            const string_value = number_to_str_lookup[original_values[i]].trim();
            if (original_values[i] < 0) {
                details[i][get_state_id()] = string_value;
                continue;
            }
            try {
                let new_value = JSON.parse(string_value);
                let skip = false;

                if (Array.isArray(new_value)) {
                    if (new_value.length === 0) {
                        details[i][get_message_id()] = string_value;
                        skip = true;
                    } else {
                        new_value = new_value[0];
                    }
                }

                if (!skip) {
                    if (typeof new_value === 'object' && !Array.isArray(new_value)) {
                        Object.entries(new_value).forEach(([key, element]) => {
                            details[i][key.toLowerCase()] = String(element);
                        });
                    } else {
                        handle_error(`unsupported type as value: ${typeof new_value}`);
                    }
                }
            } catch (e) {
                if (e instanceof SyntaxError) {
                    let result = string_value.split(";").map(x => x.trim()).filter(x => x !== "");
                    let temp_detail = {};
                    let all_are_pair = true;

                    for (let pair of result) {
                        let split_pair = pair.split(":");
                        if (split_pair.length >= 2) {
                            temp_detail[split_pair[0].toLowerCase().trim()] = split_pair.slice(1).join(":").trim();
                        } else {
                            all_are_pair = false;
                            break;
                        }
                    }

                    if (all_are_pair) {
                        Object.assign(details[i], temp_detail);
                    } else {
                        details[i][get_message_id()] = string_value;
                    }
                } else {
                    handle_error(e);
                }
            }
        }
    }
    return details;
}

// --- get_canvas helper ---
function interactive_canvas_event_func(data, event, debouncing = true) {
    data.interactive.event = {};
    if (data.interactive.initial_call) {
        data.interactive.event = event;
    } else {
        const rect = data.html.interactive_canvas.getBoundingClientRect();
        data.interactive.event.x = event.clientX - rect.left;
        data.interactive.event.y = event.clientY - rect.top;
    }

    // --- update data.event ---
    data.interactive.event.on_right_padding = data.interactive.event.on_right_padding ?? data.interactive.event.x > (data.canvas.inner_width + data.canvas.padding.left);
    data.interactive.event.on_left_padding = data.interactive.event.on_left_padding ?? data.interactive.event.x < data.canvas.padding.left;
    data.interactive.event.on_top_padding = data.interactive.event.on_top_padding ?? data.interactive.event.y < data.canvas.padding.top;
    data.interactive.event.on_bottom_padding = data.interactive.event.on_bottom_padding ?? data.interactive.event.y > (data.canvas.inner_height + data.canvas.padding.top);
    if (data.interactive.event.on_right_padding && !data.interactive.event.on_bottom_padding &&
        !data.interactive.event.on_left_padding && !data.interactive.event.on_top_padding) {
        data.interactive.event.index_factor = 1;
        data.interactive.event.on_right_padding = false;
    }
    data.interactive.event.on_padding = data.interactive.event.on_padding ?? (data.interactive.event.on_left_padding || data.interactive.event.on_right_padding || data.interactive.event.on_top_padding || data.interactive.event.on_bottom_padding);
    data.interactive.event.relative_x = data.interactive.event.relative_x ?? Math.min((Math.max(data.canvas.padding.left, data.interactive.event.x) - data.canvas.padding.left), data.canvas.inner_width);
    data.interactive.event.relative_y = data.interactive.event.relative_y ?? Math.min((Math.max(data.canvas.padding.top, data.interactive.event.y) - data.canvas.padding.top), data.canvas.inner_height);
    data.interactive.event.index_factor = data.interactive.event.index_factor ?? data.interactive.event.relative_x / data.canvas.inner_width;
    data.interactive.event.value_factor = data.interactive.event.value_factor ?? 1 - (data.interactive.event.relative_y / data.canvas.inner_height);
    data.interactive.event.selected_value = data.interactive.event.selected_value ?? data.interactive.event.value_factor * (data.datastream.max_value - data.datastream.min_value) + data.datastream.min_value;
    data.interactive.event.current_index = data.interactive.event.current_index ?? get_index_from_factor(data.datastream.time, data.interactive.event.index_factor, data.settings['discreteness']);
    data.interactive.event.current_index_x = data.interactive.event.current_index_x ?? get_factor_from_index(data.datastream.time, data.interactive.event.current_index) * data.canvas.inner_width + data.canvas.padding.left;
    if (data.datastream.min_value === data.datastream.max_value) {
        data.interactive.event.current_index_y = data.interactive.event.current_index_y ?? data.canvas.padding.top + data.canvas.inner_height * 0.5;
    } else {
        data.interactive.event.current_index_y = data.interactive.event.current_index_y ?? data.canvas.inner_height - (((data.datastream.value[data.interactive.event.current_index] - data.datastream.min_value) / (data.datastream.max_value - data.datastream.min_value)) * data.canvas.inner_height) + data.canvas.padding.top;
    }

    // --- reset interactive canvas ---
    data.interactive.context.clearRect(0, 0, data.html.interactive_canvas.width, data.html.interactive_canvas.height)
    const previous_line_width = data.interactive.context.lineWidth;
    data.interactive.context.lineWidth = 2;

    // --- draw time identification tool ---
    if (data.settings['identification_tool'] !== 'Off' && !data.interactive.event.on_padding && (!data.interactive.initial_call || data.interactive.on_initial_tool_time)) {
        // draw the line
        data.interactive.context.strokeStyle = 'rgba(255, 0, 0, 0.45)';
        data.interactive.context.beginPath();
        data.interactive.context.moveTo(data.interactive.event.current_index_x, data.canvas.padding.top);
        data.interactive.context.lineTo(data.interactive.event.current_index_x, data.canvas.inner_height + data.canvas.padding.top);
        data.interactive.context.stroke();

        // set the font for consistency with the axis labels
        data.interactive.context.strokeStyle = data.canvas.color.tool;
        data.interactive.context.font = '12px Arial';
        data.interactive.context.textAlign = 'center';
        data.interactive.context.textBaseline = 'middle';

        // compute position and content of the label
        let labelText = timestamp_to_label(data.datastream.time[data.interactive.event.current_index], data);
        let labelWidth = data.canvas.max_time_label_width;
        let labelX = data.interactive.event.current_index_x - labelWidth / 2;
        let labelY = data.settings['display_x-axis'] !== 'Off' ? data.canvas.inner_height + data.canvas.padding.top + 20 : data.canvas.inner_height + data.canvas.padding.top;

        if (data.settings['time_axis_format'] === 'Absolute') {
            let [date_part, time_part] = timestamp_to_label(data.datastream.time[data.interactive.event.current_index], data);

            // draw label background
            data.interactive.context.fillStyle = data.canvas.color.background;
            data.interactive.context.fillRect(labelX, labelY - 10, labelWidth, 35);

            // draw date
            data.interactive.context.fillStyle = data.canvas.color.tool;
            data.interactive.context.textAlign = 'center';
            data.interactive.context.textBaseline = 'bottom';
            data.interactive.context.fillText(date_part, labelX + labelWidth / 2, labelY + 10);

            // draw time
            data.interactive.context.textBaseline = 'top';
            data.interactive.context.fillText(time_part, labelX + labelWidth / 2, labelY + 10);
        } else {
            // draw label background
            data.interactive.context.fillStyle = data.canvas.color.background;
            data.interactive.context.fillRect(labelX - 5, labelY - 10, labelWidth + 10, 20);

            // insert text into the label
            data.interactive.context.fillStyle = data.canvas.color.tool;
            data.interactive.context.fillText(labelText, labelX + labelWidth / 2, labelY);
        }
    }

    // --- draw value identification tool ---
    if (data.settings['identification_tool'] !== 'Off' && !data.interactive.event.on_padding && (!data.interactive.initial_call || data.interactive.on_initial_tool_value)) {
        // draw the Linie
        data.interactive.context.strokeStyle = 'rgba(255, 0, 0, 0.45)';
        data.interactive.context.beginPath();
        data.interactive.context.moveTo(data.canvas.padding.left, data.interactive.event.current_index_y);
        data.interactive.context.lineTo(data.canvas.padding.left + data.canvas.inner_width, data.interactive.event.current_index_y);
        data.interactive.context.stroke();

        // compute position and content of the label
        data.interactive.context.strokeStyle = data.canvas.color.tool;
        let valueText = value_to_label(data.datastream.value[data.interactive.event.current_index]);
        let valueLabelWidth = data.interactive.context.measureText(valueText).width + 10;

        // draw label background
        data.interactive.context.fillStyle = data.canvas.color.background;
        data.interactive.context.fillRect(data.canvas.padding.left - 8 - valueLabelWidth, data.interactive.event.current_index_y - 8, valueLabelWidth, 20);

        // insert text into the label
        data.interactive.context.textAlign = 'right';
        data.interactive.context.fillStyle = data.canvas.color.tool;
        data.interactive.context.fillText(valueText, data.canvas.padding.left - 8, data.interactive.event.current_index_y + 3);

    }

    // --- restore previous line width ---
    data.interactive.context.setLineDash([]);
    data.interactive.context.lineWidth = previous_line_width

    // --- fill details window ---
    if (data.settings['display_details_window'] !== 'Off' && !data.interactive.event.on_padding && (!data.interactive.initial_call || data.interactive.on_initial_details)) {
        data.html.details_element.style.backgroundColor = data.canvas.color.background;
        data.html.details_element.style.color = data.canvas.color.text;
        remove_all_children(data.html.details_element)
        const details = data.details[data.interactive.event.current_index];

        // Create and append new elements using classes for styling
        const elements = Object.keys(details);
        elements.forEach((element) => {
            if (details[element] !== undefined) {
                const title = document.createElement('p');
                title.style.fontWeight = 'bold';
                title.textContent = element.split('_').map(word => word.charAt(0).toUpperCase() + word.slice(1)).join(' ') + ":";

                const value = document.createElement('p');
                value.textContent = details[element];

                const container = create('<div></div>', [
                    title,
                    value
                ]);

                data.html.details_element.appendChild(container);
            }
        });
    }

    // --- call event listener ---
    if (data.interactive.func && (!data.interactive.initial_call || data.interactive.on_initial_func)) {
            data.interactive.func(data);
    }
}


// --- dropdown selection complete functions ---
function on_selection_complete_uuid(state) {
    selected_sorted_datastream_keys = [];
    const dropdown_content = document.getElementById('uuids_content');
    const selected_items = dropdown_content.querySelectorAll('.selected');
    selected_sorted_datastream_keys = Array.from(selected_items).map(item => item.textContent.replace("\u26A1", ""));
    // --- memorize the toggled-uuids state ---
    if (!(state.initial)) {
        state.changes.forEach(uuid => {
        if (selected_sorted_datastream_keys.includes(uuid)) {
            toggled_uuids[uuid] = 'on';
        } else {
            toggled_uuids[uuid] = 'off';
        }
        });
    }
    // --- recompute the chart ---
    recompute_data();
}

function on_selection_complete_computation_method(state) {
    if (state.changes.size > 0) {
        recompute_data();
    }
}
function on_selection_complete(state) {
    if (state.changes.size > 0) {
        initialize_server_connection();
        recompute_data();
    }
}

// --- computation helper functions ---
function expand_array(arr, new_size, max_value = null) {
    if (new_size === arr.length) {
        return arr;
    }
    return Array.from({length: new_size}, (_, i) => i < arr.length ?
        arr[i] : (max_value !== null ? (max_value - arr[arr.length - 1]) / (new_size - arr.length) * (i - arr.length) + arr[arr.length - 1] : null));
}

// --- detail helper functions ---
function value_to_label(value) {
    if (typeof value === 'number') {
        if (value >= Math.pow(10, 27)) return "∞";
        if (value <= - Math.pow(10, 27)) return "-∞";
        // calculate fraction digits
        let fractionDigits;
        const absolute_value = Math.abs(value);
        if (value <= 1 && value >= -1) fractionDigits = 2;
        else if(absolute_value >= Math.pow(10, 26)) {
            fractionDigits = 0;
        }
        else {
            const int_value_length = Math.floor(Math.log10(absolute_value)) + 1;
            const mod = int_value_length % 3;
            if (mod === 0) fractionDigits = 0;
            else if (mod === 1) fractionDigits = 2;
            else if (mod === 2) fractionDigits = 1;
        }

        // calculate label
        if (absolute_value >= Math.pow(10, 24)) {
            return (value / Math.pow(10, 24)).toFixed(fractionDigits) + 'Y';
        } else if (absolute_value >= Math.pow(10, 21)) {
            return (value / Math.pow(10, 21)).toFixed(fractionDigits) + 'Z';
        } else if (absolute_value >= Math.pow(10, 18)) {
            return (value / Math.pow(10, 18)).toFixed(fractionDigits) + 'E';
        } else if (absolute_value >= Math.pow(10, 15)) {
            return (value / Math.pow(10, 15)).toFixed(fractionDigits) + 'P';
        } else if (absolute_value >= Math.pow(10, 12)) {
            return (value / Math.pow(10, 12)).toFixed(fractionDigits) + 'T';
        } else if (absolute_value >= Math.pow(10, 9)) {
            return (value / Math.pow(10, 9)).toFixed(fractionDigits) + 'G';
        } else if (absolute_value >= Math.pow(10, 6)) {
            return (value / Math.pow(10, 6)).toFixed(fractionDigits) + 'M';
        } else if (absolute_value >= Math.pow(10, 3)) {
            return (value / Math.pow(10, 3)).toFixed(fractionDigits) + 'K';
        } else {
            return value.toFixed(fractionDigits) + '  ';
        }
    }
    if (value !== undefined && value !== null) {
        console.error("this value was not a number and not undefined and not null: " + value, "typeof value: '" + typeof value, "'");
    }

    return 'NaN';
}

function timestamps_to_seconds(timestamps) {
    const now = new Date().getTime();
    const oneYearFromNow = now + (365 * 24 * 60 * 60 * 1000);
    return timestamps.map(timestamp => {
        const asSeconds = new Date(timestamp * 1000).getTime();
        if (asSeconds <= oneYearFromNow) {
            return timestamp;
        } else {
            return timestamp / 1000;
        }
    });
}

function timestamp_to_label(timestamp, data, detail = false) {
    if (data.settings['time_axis_format'] === 'Absolute') {
        const date = new Date(timestamp * 1000);
        const date_part = date.toLocaleDateString(); // todo different time zones from settings
        const time_part = date.toLocaleTimeString();
        if (detail) {
            return `${date_part} ${time_part}`;
        }
        return [`${date_part}`, `${time_part}`];
    } else {
        const first_timestamp = data.datastream.time[0];
        const max_time = data.datastream.time[data.datastream.time.length - 1] - first_timestamp;
        let time = timestamp - first_timestamp;
        const msec = Math.floor(time * 1000) % 1000;
        const seconds = Math.floor(time) % 60;
        const minutes = Math.floor(time / 60) % 60;
        const hours = Math.floor(time / (60 * 24)) % 24;
        const days = Math.floor(time / (60 * 24 * 365)) % 30;
        const months = Math.floor(time / (60 * 24 * 365 * 12)) % 12;
        const years = Math.floor(time / (60 * 24 * 365));

        if (detail) {
            const max_amount_decimals = data.settings['max_amount_decimals'] !== undefined && data.settings['max_amount_decimals'] !== '' ? parseInt(data.settings['max_amount_decimals']) : -1;
            if (time === 0) {
                return '0s';
            }
            let return_value = '';
            if (years !== 0) {
                return_value += `${years}y `;
            }
            if (months !== 0) {
                return_value += `${months}mn `;
            }
            if (days !== 0) {
                return_value += `${days}d `;
            }
            if (hours !== 0) {
                return_value += `${hours}h `;
            }
            if (minutes !== 0) {
                return_value += `${minutes}m `;
            }
            if (time % 60 !== 0) {
                if (max_amount_decimals < 0) {
                    return_value += `${time % 60}s `;
                } else {
                    if (max_amount_decimals === 1 || max_amount_decimals === 2) {
                        return_value += `${(time % 60).toFixed(max_amount_decimals)}s `;
                    } else {
                        return_value += `${seconds}s `;
                    }
                    if (msec !== 0 && max_amount_decimals >= 3) {
                        if (max_amount_decimals === 3) {
                            return_value += `${msec}ms `;
                        } else {
                            return_value += `${((time * 1000) % 1000).toFixed(max_amount_decimals - 3)}`.replace(/\.0+$|(\.\d*?[1-9])0+$/, '$1') + `ms `;
                        }
                    }
                }
            }
            return return_value.slice(0, -1);
        }

        if (years !== 0) {
            return `${years}y ${months}mn`;
        } else if (months !== 0) {
            if (max_time > 31536000) {
                return `${months}mn`;
            } else {
                return `${months}mn ${days}d`;
            }
        } else if (days !== 0) {
            if (max_time > 2628000) {
                return `${days}d`;
            } else {
                return `${days}d ${hours}h`;
            }
        } else if (hours !== 0) {
            if (max_time > 86400) {
                return `${hours}h`;
            } else {
                return `${hours}h ${minutes}m`;
            }
        } else if (minutes !== 0) {
            if (max_time > 3600) {
                return `${minutes}m`;
            } else {
                return `${minutes}m ${seconds}s`;
            }
        } else if (seconds !== 0) {
            if (max_time > 60) {
                return `${seconds}s`;
            } else {
                return `${seconds}s ${msec}ms`;
            }
        } else {
            return `${msec}ms`;
        }
    }
}

function get_max_label_width(data) {
    if (data.settings['time_axis_format'] === 'Absolute') {
        return 68;
    } else {
        const timestamp = data.datastream.time[data.datastream.time.length - 1] - data.datastream.time[0] || 0;
        const years = Math.floor(timestamp / (60 * 60 * 24 * 365));
        const significant_digits = `${years}`.length;
        return Math.max(4, significant_digits + 2) * 6.673828125 + 22.669921875;    // for "normal 12px Arial"
    }
}

// --- error handling ---
function handle_error(...args) {
    color_light.text = 'red';
    color_dark.text = 'red';
    recompute_data();
    console.error(args);
}

// --- array access helpers ---
function get_factor_from_index(times, index, average_length = undefined) {
    // --- special cases ---
    if (index === 0) {return 0;}
    if (index === times.length - 1) {return 1;}

    // --- variables ---
    const computation_method = params.get('computation_method');
    const dividend = computation_method === 'Duration' ? (times[index] - times[0]) : index;
    if (average_length === undefined) {
        average_length = computation_method === 'Duration' ? (times[times.length - 1] - times[0]) : (times.length - 1);
    }

    // --- cleanup ---
    return dividend / average_length;
}

function get_index_from_factor(times, factor, settings_discreteness = undefined) {
    // --- special cases ---
    if (times.length === 0) {
        return undefined;
    }
    if (times.length === 1) {
        return 0;
    }
    if (factor === 1) {
        return times.length - 1;
    }
    if (params.get("computation_method") === 'Equidistant') {
        if (get_discreteness(settings_discreteness) === 0) {  // meaning it is continuous
            return Math.round(factor * (times.length - 1));
        }
        return Math.floor(factor * (times.length - 1));
    }

    // --- variables ---
    const min_timestamp = times[0];
    const max_timestamp = times[times.length - 1];
    const max_duration = max_timestamp - min_timestamp;
    const searched_timestamp = factor * max_duration;
    let low = 0, high = times.length - 1, higher = times.length;

    // --- computation of solution for computation method 'Duration' ---
    while (low <= high) {
        let mid = Math.floor((low + high) / 2);
        if (times[mid] - min_timestamp <= searched_timestamp) {
            low = mid + 1;
        } else {
            higher = mid;
            high = mid - 1;
        }
    }

    // --- cleanup ---
    if (higher === times.length) {
        return times.length - 1;
    } else if (get_discreteness(settings_discreteness) !== 0) {  // meaning it is discrete
        return higher - 1;
    } else if (higher === 0) {
        return 0;
    } else {
        const middle_timestamp = ((times[higher - 1] - min_timestamp) + (times[higher] - min_timestamp)) / 2;
        if (searched_timestamp < middle_timestamp) {
            return higher - 1;
        } else {
            return higher;
        }
    }
}

function get_index_from_index(source_times, index, target_times, average_length = undefined, settings_discreteness = undefined) {
    // --- special cases ---
    if (params.get('computation_method') === 'Equidistant' && source_times.length === target_times.length) {
        return index;   // this is to prevent a rounding bug reasoned in machine accuracy
    }

    // --- cleanup ---
    return get_index_from_factor(target_times, get_factor_from_index(source_times, index, average_length), settings_discreteness);
}

// --- other helper functions ---
function get_average_duration(uuids, computation_method = undefined) {
    if (computation_method === undefined) {
        computation_method = params.get('computation_method');
    }
    let duration_sum = 0;
    for (let i = 0; i < uuids.length; i++) {
        const times = datastreams[uuids[i]]['time'];

        duration_sum += computation_method === 'Duration' ? (times[times.length - 1] - times[0]) : times.length;
    }
    return duration_sum / uuids.length;
}

function set_selected_sorted_datastream_keys() {
    // --- variables ---
    const computation_method = params.get('computation_method');
    const settings = get_settings();
    let length_deviation = settings['outlyer_length_deviation'];
    let detected_as_outlyer = [];
    const uuids_content = document.getElementById('uuids_content');

    // --- set uuids_content children, if required ---
    if (uuids_content.children.length !== sorted_datastream_keys.length) {
        remove_all_children(uuids_content);
        set_uuids_content(sorted_datastream_keys);
    }


    // --- compute selected_sorted_datastream_keys ---
    if (length_deviation === '') {
        selected_sorted_datastream_keys = sorted_datastream_keys;
    } else {
        length_deviation = parseFloat(length_deviation) / 100;
        const average_duration = get_average_duration(sorted_datastream_keys)
        const min_duration = average_duration * (1 - length_deviation);
        const max_duration = average_duration * (1 + length_deviation);
        selected_sorted_datastream_keys = [];
        sorted_datastream_keys.forEach((uuid) => {
            const current_length = computation_method === 'Duration' ? (datastreams[uuid]['time'][datastreams[uuid]['time'].length - 1] - datastreams[uuid]['time'][0]) : datastreams[uuid]['time'].length;
            if (current_length >= min_duration && current_length <= max_duration) {
                selected_sorted_datastream_keys.push(uuid);
            } else {
                detected_as_outlyer.push(uuid);
            }
        })
    }

    // --- update uuids dropdown ---
    const buttons = Array.from(document.getElementById('uuids_content').children);
    buttons.forEach((button) => {
        const text = button.textContent.replace("\u26A1", "");
        // toggle selected to match selected_sorted_datastream_keys
        if (selected_sorted_datastream_keys.includes(text)) {
            if (toggled_uuids[text] !== 'off') {
                button.classList.add('selected');
            }
        } else {
            if (toggled_uuids[text] !== 'on') {
                button.classList.remove('selected');
            }
        }
        // set lightning-bolt if needed
        if (detected_as_outlyer.includes(text)) {
            button.textContent = "\u26A1" + text;
        } else {
            button.textContent = text;
        }
    });

    // --- update button text ---
    document.getElementById('uuids').textContent = `${document.getElementById('uuids_content').querySelectorAll('.selected').length}/${sorted_datastream_keys.length} Selected`;
}

function set_discreteness(new_discreteness) {
    if (discreteness === undefined || discreteness === 1) discreteness = new_discreteness;
    else if (new_discreteness === 1) {}
    else if (discreteness === 0) discreteness = new_discreteness;
    else if (new_discreteness === 2) discreteness = 2;
}

// --- computation functions ---
function interpolate(array, target_length) {
    // variables
    if (target_length <= 0) return array;  // if target_length is 0, return the original array (to work with settings)
    if(target_length === 1) return [array[0]];
    const output = []
    const step = (array.length - 1) / (target_length - 1);

    // compute resampled
    for (let i = 0; i < target_length; i++) {
        let index = Math.floor(i * step);
        output.push(array[index]);
    }

    // cleanup
    return output;
}

function set_name_content(options) {
    append_dropdown_options(
        document.getElementById("name"),
        document.getElementById("name_content"),
        params.get('name'),
        options,
        true,
        'name',
        false,
        (subbutton) => {
            url_dropdown_listener('name', subbutton);
            remove_all_children(document.getElementById("uuids_content"));
            const key = params.get('name');
            if (key !== undefined && key in streams) {
                set_uuids_content(streams[key].map(tuple => tuple[0]));
            }
        }
    );
}

function set_uuids_content(options) {
    // --- variables ---
    const content = document.getElementById("uuids_content");
    const button = document.getElementById("uuids");

    // --- set content ---
    append_dropdown_options(
        button,
        content,
        'All Selected',
        options,
        false,
        'uuids',
        true,
        (_) => {},
        true
    );
}

// ---- EOF ------------------------------------------------------------------------------------------------------------

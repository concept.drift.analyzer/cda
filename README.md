# Concept Drift Analyzer

## Beschreibung
Diese Website ermöglicht das Hochladen von Daten und deren aufbereitete Anzeige, um insbesondere Concept Drifts zu erkennen. Sie bietet Personalisierungsoptionen über Einstellungen und erlaubt die Live-Einspeisung von Daten über einen Port. Angefügte Dateien sind ebenfalls Bestandteil der Funktionalität.

## Features
- **Daten-Upload:** Einfaches Hochladen von Daten zur Analyse.
- **Datenaufbereitung:** Automatisierte Aufbereitung und Visualisierung der hochgeladenen Daten.
- **Concept Drift Erkennung:** Spezielle Tools zur Identifikation von Concept Drifts.
- **Personalisierung:** Anpassung der Website-Einstellungen für eine personalisierte Erfahrung.
- **Live-Daten:** Möglichkeit zur Live-Einspeisung von Daten über einen definierten Port.

## Installation

### Voraussetzungen
- Python 3.10

### Schritte zur Installation

1. Klone das Repository:
    ```sh
    git clone <repository-url>
    cd <repository-ordner>
    ```

2. Erstelle und aktiviere eine virtuelle Umgebung:
    ```sh
    python3 -m venv venv
    source venv/bin/activate
    ```

3. Installiere die Abhängigkeiten:
    ```sh
    pip install -r requirements.txt
    ```

## Nutzung

### Starten des Servers

1. Führe das Setup-Skript aus, um die Datenbank einzurichten:
    ```sh
    sqlite3 concept_drift_analyzer.db < setup.sql
    ```

2. Starte den Webserver:
    ```sh
    uvicorn webserver:app --reload
    ```

### Live-Daten Einspeisung

1. Starte den Datenintegrationsserver:
    ```sh
    python data_integration_server.py
    ```

2. Sende Daten an den entsprechenden Port.

### Anpassung der Einstellungen
Passe die Konfigurationsdatei `config.py` an, um die Website gemäß deinen Anforderungen zu personalisieren.

## Dateien

- `requirements.txt` - Liste der Python-Abhängigkeiten.
- `webserver.py` - Hauptskript für den Webserver.
- `main.py` - Hauptanwendungsskript.
- `data_integration_server.py` - Skript für die Live-Datenintegration.
- `database.py` - Skript zur Datenbankverwaltung.
- `config.py` - Konfigurationsdatei.
- `computation.py` - Skript zur Durchführung von Berechnungen.
- `basics.py` - Basisfunktionen und -klassen.
- `setup.sql` - SQL-Skript zur Einrichtung der Datenbank.
- `test_main.py` - Tests für die Hauptanwendung.
- `main.html` - Haupt-HTML-Datei.
- `js-yaml.min.js` - JavaScript-Bibliothek für YAML.
- `view.mjs` - Modul für die Ansicht.
- `upload.mjs` - Modul für den Upload.
- `settings.mjs` - Modul für die Einstellungen.
- `main.mjs` - Haupt-JavaScript-Modul.
- `config.js` - JavaScript-Konfigurationsdatei.
- `basics.js` - Basis-JavaScript-Funktionen.
- `favicon.ico` - Favicon für die Website.
- `main.css` - Haupt-CSS-Datei.
- `light_mode.css` - CSS-Datei für den Light Mode.
- `dark_mode.css` - CSS-Datei für den Dark Mode.

## Abhängigkeiten

Die in der Datei `requirements.txt` aufgeführten Abhängigkeiten sind notwendig:
```plaintext
python-dateutil~=2.8.2
icecream~=2.1.3
fastapi~=0.109.2
numpy~=1.26.3
uvicorn~=0.27.1
scipy~=1.12.0
```
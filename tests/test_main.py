# --- imports ----------------------------------------------------------------------------------------------------------
import asyncio
import aiohttp
from aiohttp import FormData
from json import dumps
from datetime import datetime
from typing import Union, Type, Tuple, List


# --- constants --------------------------------------------------------------------------------------------------------
url = "http://[::1]:54165/upload_data"
next_timestamp = 0.1
iteration = 3


# --- helper functions -------------------------------------------------------------------------------------------------
def generate_datapoint(value_type: Type[Union[float, int, str]] = float) -> Tuple[str, Union[float, int, str]]:
    global next_timestamp
    next_timestamp += 1
    return str(datetime.fromtimestamp(1700000000 + next_timestamp)), value_type(next_timestamp)
    ############################


def generate_datapoints(amount: int, value_type: Type[Union[float, int, str]] = float) \
        -> List[Tuple[str, Union[float, int, str]]]:
    global next_timestamp
    next_timestamp = iteration * 100 + 0.1
    return [generate_datapoint(value_type) for _ in range(amount)]
    ############################


# --- main -------------------------------------------------------------------------------------------------------------
async def main():
    data = FormData()
    data.add_field('uuid', str(iteration))
    data.add_field('datastream_mapping', dumps({'test': generate_datapoints(10, float)}))

    async with aiohttp.ClientSession() as session:
        async with session.post(url, data=data) as response:
            print("Status:", response.status)
            print("Content:", await response.text())


if __name__ == '__main__':
    asyncio.run(main())

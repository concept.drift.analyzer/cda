# --- imports ----------------------------------------------------------------------------------------------------------
import unittest
from backend.computation import interpolate
from typing import List


# --- NOTE -------------------------------------------------------------------------------------------------------------
"""
This file was only introduced to showcase my testing skills. 
It is not required in this Project.
"""


# --- helper functions -------------------------------------------------------------------------------------------------
def generate_values(multiplier: int = 1):
    for i in range(1, 6):
        yield i * multiplier


# --- tests ------------------------------------------------------------------------------------------------------------
class TestInterpolate(unittest.TestCase):

    def setUp(self):
        self.times = list(generate_values())
        self.values = list(generate_values(10))
        self.max_length = 9
        self.expected_times = [1.0, 1.5, 2.0, 2.5, 3.0, 3.5, 4.0, 4.5, 5.0]

    def check_interpolation(self, method: str, expected_values: List[float] = None):
        new_times, new_values = interpolate(self.times, self.values, self.max_length, method=method)
        self.assertEqual(new_times, self.expected_times)
        self.assertEqual(new_values, expected_values)

    def test_interpolate_nearest(self):
        expected_values = [10.0, 10.0, 20.0, 20.0, 30.0, 30.0, 40.0, 40.0, 50.0]
        self.check_interpolation('nearest', expected_values)

    def test_interpolate_linear(self):
        expected_values = [10.0, 15.0, 20.0, 25.0, 30.0, 35.0, 40.0, 45.0, 50.0]
        self.check_interpolation('linear', expected_values)

    def test_interpolate_invalid_method(self):
        with self.assertRaises(ValueError):
            self.check_interpolation('quadratic')

    def test_interpolate_invalid_times(self):
        with self.assertRaises(TypeError):
            interpolate("invalid_times", self.values, self.max_length, method='linear')
        with self.assertRaises(TypeError):
            interpolate({'a': 1, 'b': 2}, self.values, self.max_length, method='linear')
        with self.assertRaises(ValueError):
            interpolate([], self.values, self.max_length, method='linear')
        with self.assertRaises(TypeError):
            interpolate([1, "two", 3], self.values, self.max_length, method='linear')

    def test_interpolate_invalid_values(self):
        with self.assertRaises(TypeError):
            interpolate(self.times, "invalid_values", self.max_length, method='linear')
        with self.assertRaises(TypeError):
            interpolate(self.times, {'a': 1, 'b': 2}, self.max_length, method='linear')
        with self.assertRaises(ValueError):
            interpolate(self.times, [], self.max_length, method='linear')
        with self.assertRaises(TypeError):
            interpolate(self.times, [10, "twenty", 30], self.max_length, method='linear')

    def test_interpolate_invalid_max_length(self):
        with self.assertRaises(TypeError):
            interpolate(self.times, self.values, "invalid_max_length", method='linear')
        with self.assertRaises(TypeError):
            interpolate(self.times, self.values, 1.3, method='linear')
        with self.assertRaises(ValueError):
            interpolate(self.times, self.values, -1, method='linear')
        with self.assertRaises(ValueError):
            interpolate(self.times, self.values, 0, method='linear')


# --- main -------------------------------------------------------------------------------------------------------------
if __name__ == '__main__':
    unittest.main()

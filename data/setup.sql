-- Create the 'Stream' table
CREATE TABLE IF NOT EXISTS Stream (
    stream_id INTEGER PRIMARY KEY AUTOINCREMENT,
    uuid TEXT,
    stream_name TEXT
);

-- Create the 'Data' table
CREATE TABLE IF NOT EXISTS Data (
    data_id INTEGER PRIMARY KEY AUTOINCREMENT,
    stream_id INTEGER REFERENCES Stream(stream_id),
    timestamp DATETIME,
    value_float REAL,
    value_int INTEGER,
    value_str TEXT,
    UNIQUE(stream_id, timestamp, value_float),
    UNIQUE(stream_id, timestamp, value_int),
    UNIQUE(stream_id, timestamp, value_str)
);



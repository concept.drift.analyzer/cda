# --- imports ----------------------------------------------------------------------------------------------------------
# --- general libraries ---
from collections import defaultdict
from http.server import SimpleHTTPRequestHandler
from json import loads
from re import search
from socket import AF_INET6
from socketserver import TCPServer
from typing import Tuple, Union, Type
from datetime import datetime, timezone
from dateutil.parser import parse

# --- own files ---
from backend.config import DIS_PORT
from backend.basics import DB_TIMESTAMP_TYPE, DB_VALUE_TYPE, VALUE
from backend.main import upload_data


# --- helper classes ---------------------------------------------------------------------------------------------------
def get_as_this_type(variable: str, type_alias: Type[VALUE]) -> Tuple[Union[str, VALUE], int]:
    if variable is None:
        return 'Missing parameter', 400
    elif type_alias == Union[int, float, str]:
        variable_int_str = None
        variable_float_str = None
        try:
            variable_int_str = str(int(variable))
        except ValueError:
            pass
        try:
            variable_float_str = str(float(variable))
        except ValueError:
            pass
        if variable_int_str == variable:
            variable = int(variable)
        elif variable_float_str == variable:
            variable = float(variable)
    elif type_alias == datetime:
        variable = parse(variable)
        variable = variable.astimezone(timezone.utc)
        variable = variable.replace(tzinfo=None)
    else:
        try:
            variable = type_alias(variable)
        except ValueError:
            return f"{variable} is not of type {type_alias}", 400
    # --- cleanup ---
    return variable, 200
    ##############################


class CustomRequestHandler(SimpleHTTPRequestHandler):
    def do_POST(self):
        # --- retrieve data ---
        content_length = int(self.headers['Content-Length'])
        body = self.rfile.read(content_length)
        json_string = search(r'\{.*}', body.decode('utf-8')).group()
        json_dict = loads(json_string)

        # --- check uuid input ---
        uuid = json_dict.get('instance-uuid', None)
        if uuid is None:
            print("missing uuid")
            return

        # --- check datastream_mapping input ---
        datastream_mapping = defaultdict(list)
        for data in json_dict.get('datastream', []):
            if 'stream:point' in data:
                stream_point = data['stream:point']
                # stream:id
                stream_id = stream_point.get('stream:id', None)
                if stream_id is None:
                    print("stream:id is missing")
                    continue
                stream_id: str
                # stream:timestamp
                timestamp = stream_point.get('stream:timestamp', None)
                if timestamp is None:
                    print("stream:timestamp is missing")
                    continue
                timestamp, ret_code = get_as_this_type(timestamp, DB_TIMESTAMP_TYPE)
                if ret_code != 200:
                    print("stream:timestamp is invalid")
                    continue
                # stream:value
                value = stream_point.get('stream:value', None)
                if value is None:
                    print("stream:value is missing")
                    continue
                value, ret_code = get_as_this_type(value, DB_VALUE_TYPE)
                if ret_code != 200:
                    print("stream:value is invalid")
                    continue

                if stream_id is not None and timestamp is not None and value is not None:
                    datastream_mapping[stream_id].append((timestamp, value))
                else:
                    print(f"At least one of the following is missing: "
                          f"'stream:id'={stream_id}, 'stream:timestamp'={timestamp}, 'stream:value'={value}")
        upload_data(uuid, datastream_mapping)
        #############################
    #########################################


def start_data_integration_server() -> None:
    # IPv6 Local Loopback
    host, port = "::1", DIS_PORT

    class TCPServerV6(TCPServer):
        address_family = AF_INET6

    with TCPServerV6((host, port), CustomRequestHandler, bind_and_activate=False) as httpd:
        httpd.allow_reuse_address = True
        httpd.server_bind()
        httpd.server_activate()
        print(f"Server listening on {host}:{port}")
        httpd.serve_forever()


# --- main -------------------------------------------------------------------------------------------------------------
if __name__ == "__main__":
    start_data_integration_server()

# --- imports ----------------------------------------------------------------------------------------------------------
# --- general libraries ---
from typing import Tuple, List
from scipy import interpolate as scipy_interpolate
import numpy as np

# --- own files ---
from backend.basics import DB_VALUE_TYPE, TIMESTAMP_TYPE


# --- helper functions -------------------------------------------------------------------------------------------------
def interpolate(times: List[TIMESTAMP_TYPE], values: List[DB_VALUE_TYPE], max_length: int, method: str = 'nearest') \
        -> Tuple[List[TIMESTAMP_TYPE], List[DB_VALUE_TYPE]]:
    f = scipy_interpolate.interp1d(times, values, kind=method)
    new_timestamps = np.linspace(times[0], times[-1], max_length)
    new_values = f(new_timestamps)
    return list(new_timestamps), list(new_values)


# --- NOTE -------------------------------------------------------------------------------------------------------------
"""
For deployment use the code above.
The following code is the solution to the test_computation.py test file.
This is not required in this project and should just showcase my testing skills.
All checks to verify the data correctness are done higher in the program's hierarchy.
"""


# --- global variables -------------------------------------------------------------------------------------------------
# allowed_interpolate_methods = ['nearest', 'linear']


# --- helper functions -------------------------------------------------------------------------------------------------
"""
def interpolate(times: List[TIMESTAMP_TYPE], values: List[DB_VALUE_TYPE], max_length: int, method: str = 'nearest') \
        -> Tuple[List[TIMESTAMP_TYPE], List[DB_VALUE_TYPE]]:
    # Input validations
    if not isinstance(times, list) or not all(isinstance(t, (int, float)) for t in times):
        raise TypeError("times must be a list of numbers.")
    if not isinstance(values, list) or not all(isinstance(v, (int, float)) for v in values):
        raise TypeError("values must be a list of numbers.")
    if not isinstance(max_length, int):
        raise TypeError("max_length must be an integer.")
    if max_length <= 0:
        raise ValueError("max_length must be a positive integer.")
    if len(times) != len(values):
        raise ValueError("times and values must be of the same length.")
    if not isinstance(method, str):
        raise TypeError("method must be a string.")
    if method not in allowed_interpolate_methods:
        raise ValueError(f"method must be one of {allowed_interpolate_methods}.")
    try:
        f = scipy_interpolate.interp1d(times, values, kind=method)
    except ValueError as e:
        raise ValueError(f"Invalid method '{method}': {e}")

    new_timestamps = np.linspace(times[0], times[-1], max_length)
    new_values = f(new_timestamps)
    return list(new_timestamps), list(new_values)
"""


# --- main functions ---------------------------------------------------------------------------------------------------

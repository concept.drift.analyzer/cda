# --- imports ----------------------------------------------------------------------------------------------------------
# --- general libraries ---
from collections import defaultdict
from contextlib import asynccontextmanager
from queue import Queue, Empty, Full
from sqlite3 import Cursor, connect
from typing import Union, List, Optional, Tuple, Dict
from asyncio import Lock
from icecream import ic


# --- own files ---
from backend.computation import interpolate
from backend.basics import (DB_STREAM_ID_TYPE, DB_STREAM_NAME_TYPE, DB_TIMESTAMP_TYPE, DB_VALUE_TYPE, DB_UUID_TYPE,
                            DB_DATASTREAM_TYPE, parse_timestamp, DATASTREAMS_TYPE, Discreteness)
from backend.config import DATABASE

# --- Constants --------------------------------------------------------------------------------------------------------
db_lock = None


# --- helper functions -------------------------------------------------------------------------------------------------
def get_cursor() -> Cursor:
    """
    Gets a cursor from the database.

    :return: A cursor from the database.
    """
    new_cursor = connect(DATABASE).cursor()
    return new_cursor


def release_cursor(cursor: Cursor) -> None:
    """
    Releases a cursor to the database back to the pool.

    :param cursor: The cursor to release.
    """
    if not isinstance(cursor, Cursor):
        return
    cursor.connection.close()


async def acquire_db_lock() -> None:
    """
    Acquires the database lock.

    :return: None
    """
    # --- variables ---
    global db_lock

    # --- create lock if it doesn't exist ---
    if db_lock is None:
        db_lock = Lock()

    # --- cleanup ---
    await db_lock.acquire()
    ########################


def release_db_lock() -> None:
    """
    Releases the database lock.

    :return: None
    """
    # --- variables ---
    global db_lock

    # --- cleanup ---
    if isinstance(db_lock, Lock):
        db_lock.release()
    ###################


@asynccontextmanager
async def db_locked():
    """
    Context manager for acquiring and releasing the database lock.

    Usage:
        with db_locked():
            # Perform database operations here
    """
    # --- acquire lock ---
    await acquire_db_lock()

    # --- cleanup ---
    try:
        yield
    finally:
        release_db_lock()
    #####################


def run_query(query: str, parameters: Union[List[tuple], tuple, None] = None, cursor: Optional[Cursor] = None,
              fetch_all: Optional[bool] = None) -> Union[List[tuple], tuple, None]:
    """
    Runs a query on the database and handles exceptions.
    ATTENTION: May raise DatabaseError.

    :param cursor:      the cursor to run the query on.
    :param query:       the query to run.
    :param parameters:  the parameters for the query.
    :param fetch_all:   if True, fetches all results, otherwise only the first result
    :return:            the result of the query.
    """
    created_cursor = False
    try:
        if cursor is None:
            cursor = get_cursor()
            created_cursor = True
        if isinstance(parameters, list):
            cursor.executemany(query, parameters)
        elif parameters is not None:
            cursor.execute(query, parameters)
        else:
            cursor.execute(query)
        if fetch_all is True:
            return cursor.fetchall()
        elif fetch_all is False:
            return cursor.fetchone()
        else:
            return None
    finally:
        if created_cursor:
            cursor.connection.close()
    #################


def insert_data(cursor: Cursor, stream_id: DB_STREAM_ID_TYPE,
                timestamp_value_pairs: List[Tuple[DB_TIMESTAMP_TYPE, DB_VALUE_TYPE]]) -> Discreteness:
    """
    Inserts data into the database.

    :param cursor:                  The cursor of the database.
    :param stream_id:               The ID of the corresponding stream.
    :param timestamp_value_pairs:   A list of timestamp-value pairs to be inserted.
    :return:                        The discreteness of the inserted data.
    """
    # --- computation ---
    query = ('INSERT OR IGNORE INTO Data (stream_id, timestamp, value_float, value_int, value_str) '
             'VALUES (?, ?, ?, ?, ?);')
    parameters = []
    float_values = 0
    string_values = 0
    for timestamp, value in timestamp_value_pairs:
        if isinstance(value, float):
            value = (value, None, None)
            float_values += 1
        elif isinstance(value, int):
            value = (None, value, None)
        else:
            value = (None, None, value)
            string_values += 1
        parameters.append((stream_id, timestamp, *value))
    run_query(query, parameters, cursor)

    # --- return discreteness ---
    if string_values > 0:
        return Discreteness.STRINGS_AND_INTS
    elif float_values == 0:
        return Discreteness.ONLY_INTS
    else:
        return Discreteness.FLOATS_AND_INTS
    ######################


def insert_stream(cursor: Cursor, uuid: DB_UUID_TYPE, stream_name: DB_STREAM_NAME_TYPE) \
        -> Tuple[DB_STREAM_ID_TYPE, bool]:
    """
    Inserts a stream into the database.

    :param cursor:          The cursor of the database.
    :param uuid:            The UUID of the stream.
    :param stream_name:     The name of the stream.
    :return:                The ID of the stream. And a boolean indicating if the datastream already existed.
    """
    # --- computation ---
    query = "SELECT stream_id FROM Stream WHERE uuid = ? AND stream_name = ?"
    parameters = (uuid, stream_name)
    result = run_query(query, parameters, cursor, fetch_all=False)
    did_already_exist = True
    # --- insert if it doesn't exist ---
    if result is None:
        did_already_exist = False
        query = "INSERT INTO Stream (uuid, stream_name) VALUES (?, ?)"
        run_query(query, parameters, cursor)
        query = "SELECT last_insert_rowid()"
        result = run_query(query, None, cursor, fetch_all=False)
    return result[0], did_already_exist
    ############################


# --- main functions ---------------------------------------------------------------------------------------------------
def get_streams() -> Dict[DB_STREAM_NAME_TYPE, List[Tuple[DB_UUID_TYPE, DB_TIMESTAMP_TYPE]]]:
    """
    Gets all streams from the database sorted by the earliest timestamp of their data points.

    :return: Dict of sorted uuids per stream name. {stream_name: [(uuid, min_timestamp), ...], ...}
    """
    # --- sql query ---
    sorted_stream_ids_sql = """
    SELECT s.stream_name, s.uuid, MIN(d.timestamp) AS min_timestamp
    FROM Stream s
    JOIN Data d ON s.stream_id = d.stream_id
    GROUP BY s.stream_name, s.uuid
    ORDER BY min_timestamp;
    """

    # --- Execute the query to get sorted [(stream_name, uuid), ...] ---
    sorted_streams = run_query(sorted_stream_ids_sql, fetch_all=True)

    # --- group the uuids by stream_name ---
    output = defaultdict(list)
    for stream_name, uuid, min_timestamp in sorted_streams:
        output[stream_name].append((uuid, parse_timestamp(min_timestamp)))

    # --- cleanup ---
    return output
    ################


def insert_datastream(cursor: Cursor, uuid: DB_UUID_TYPE, stream_name: DB_STREAM_NAME_TYPE,
                      datastream: DB_DATASTREAM_TYPE) -> Tuple[Discreteness, bool]:
    """
    Uploads data to the database.

    :param cursor:              The cursor of the database.
    :param uuid:                Instance.uuid
    :param stream_name:         The name of the stream.
    :param datastream:          The datastream to be uploaded. [(Data.timestamp, Data.value), ...]
    :return:                    The discreteness of the datastream.
                                And a boolean indicating if the datastream already existed.
    """
    # --- computation ---
    # insert Stream
    stream_id, did_already_exist = insert_stream(cursor=cursor, uuid=uuid, stream_name=stream_name)
    # insert Data
    return insert_data(cursor=cursor, stream_id=stream_id, timestamp_value_pairs=datastream), did_already_exist
    ########################


def get_datastreams(stream_name: DB_STREAM_NAME_TYPE, given_uuids: List[DB_UUID_TYPE], all_except_given_uuids: bool,
                    value_filter: List[float], max_length: int, live: bool) \
        -> Tuple[DATASTREAMS_TYPE, List[DB_UUID_TYPE], Discreteness]:
    """
    Retrieves data streams and formats the results into a dictionary.
    Each key is a UUID with its value being a list of tuples (timestamp, value).

    :param stream_name:             The name of the stream.
    :param given_uuids:             A list of UUIDs to include or exclude in the query.
    :param all_except_given_uuids:  If True, fetches streams not in the given_uuids list;
                                    otherwise, fetches streams in the given_uuids list.
    :param value_filter:            A list of values to remove from the data.
    :param max_length:              The maximum number of datapoints per datastream.
                                    Compact with interpolation if needed.
                                    0 means no limit.
    :param live:                    If True, never interpolates the newest datastream.
    :return:                        by min_timestamp Sorted List of Tuples of the form (UUID, [(timestamp, value), ...])
    """
    # --- variables ---
    data_streams = dict()
    ordered_uuids = []
    placeholders = ','.join('?' * len(given_uuids))
    uuid_condition = "NOT IN" if all_except_given_uuids else "IN"
    parameters = tuple([stream_name] + given_uuids)
    query = f"""
    SELECT s.uuid, d.timestamp, d.value_float, d.value_int, d.value_str
    FROM Data d
    JOIN Stream s ON d.stream_id = s.stream_id
    WHERE s.stream_name = ? AND s.uuid {uuid_condition} ({placeholders})
    ORDER BY d.timestamp
    """
    float_values = 0
    string_values = 0

    # --- get data from database ---
    result = run_query(query, parameters, None, True)

    # --- merge value types into one ---
    for uuid, timestamp, value_float, value_int, value_str in result:
        if value_float is not None:
            value = value_float
            float_values += 1
        elif value_int is not None:
            value = value_int
        else:
            value = value_str
            string_values += 1
        time = parse_timestamp(timestamp)
        if value not in value_filter:
            if uuid not in data_streams:
                data_streams[uuid] = {'time': [], 'value': []}
                if uuid not in ordered_uuids:
                    ordered_uuids.append(uuid)
            data_streams[uuid]['time'].append(time)
            data_streams[uuid]['value'].append(value)

    # --- interpolate data if needed ---
    if string_values > 0:
        discreteness = Discreteness.STRINGS_AND_INTS
    elif float_values == 0:
        discreteness = Discreteness.ONLY_INTS
    else:
        discreteness = Discreteness.FLOATS_AND_INTS
    if max_length > 0 and len(ordered_uuids) > 0:
        for uuid in (ordered_uuids[:-1] if live else ordered_uuids):
            if len(data_streams[uuid]['time']) > max_length:
                time, value = interpolate(data_streams[uuid]['time'], data_streams[uuid]['value'], max_length,
                                          'previous')
                data_streams[uuid]['time'] = time
                data_streams[uuid]['value'] = value

    # --- cleanup ---
    return data_streams, ordered_uuids, discreteness
    ###################

# --- imports ----------------------------------------------------------------------------------------------------------
# --- other libraries ---
from collections import defaultdict
from contextlib import contextmanager
from datetime import datetime, timedelta
from queue import Queue
from threading import Condition, Thread, RLock
from typing import TypeAlias, Union, TypeVar, Dict, List, Tuple, Optional, Generic, Callable, Iterable, Any
from enum import Enum, auto, IntEnum, IntFlag
import traceback
import logging

# --- own files ---
from backend.config import LOGGING_FILE, LOGGING_LEVEL, DEBUG, PRINT_LEVEL


# --- Enums ------------------------------------------------------------------------------------------------------------
class BaseEnum(Enum):
    def to_string(self):
        return self.name.lower().replace('_', ' ')
        ##############

    @classmethod
    def from_string(cls, string: str):
        name = string.upper().replace(' ', '_')
        return cls[name]
        ##############
    ##########################################


class ComputationMethod(BaseEnum, IntEnum):
    DURATION = auto()


class ResponseDetailFlags(BaseEnum, IntFlag):
    TIMES = 1
    VALUES = 2
    DETAILS = 4
    MAPPING = 8
    DRIFTS = 16


class Discreteness(BaseEnum, IntEnum):
    FLOATS_AND_INTS = 0,
    ONLY_INTS = 1,
    STRINGS_AND_INTS = 2


# --- Development Constants --------------------------------------------------------------------------------------------
# --- general ---
VALUE = TypeVar('VALUE')

# --- SQLite database types ---
DB_INSTANCE_TYPE: TypeAlias = int                                                                   # np.int64
DB_UUID_TYPE: TypeAlias = str
DB_IS_PRODUCTION_TYPE: TypeAlias = bool
DB_TIMESTAMP_TYPE: TypeAlias = datetime  # 'datetime64[us]'. Not allowed to be any float.
#                                          That would cause a bug in computation_old.py/convert_datapoint_type() under
#                                          '# --- variables ---' as isinstance(float(1.0), np.float64) is True
DB_VALUE_FLOAT_TYPE: TypeAlias = float
DB_VALUE_INT_TYPE: TypeAlias = int
DB_VALUE_STR_TYPE: TypeAlias = str
DB_VALUE_TYPE: TypeAlias = Union[DB_VALUE_FLOAT_TYPE, DB_VALUE_INT_TYPE, DB_VALUE_STR_TYPE]
DB_STREAM_ID_TYPE: TypeAlias = int                                                                  # np.int64
DB_STREAM_NAME_TYPE: TypeAlias = str
DB_DATASTREAM_TYPE: TypeAlias = List[Tuple[DB_TIMESTAMP_TYPE, DB_VALUE_TYPE]]
DB_DATASTREAM_MAPPING_TYPE: TypeAlias = Dict[DB_STREAM_NAME_TYPE, DB_DATASTREAM_TYPE]

# --- main.py ---
SESSION_ID_TYPE: TypeAlias = str
USER_DICT_TYPE: TypeAlias = Dict[str, Union[
    str,
    List[DB_STREAM_ID_TYPE],
    DB_STREAM_NAME_TYPE,
    bool,
    Dict[DB_STREAM_ID_TYPE, int],
    ResponseDetailFlags,
    ComputationMethod
]]

# --- webserver.py ---
STATUS_CODE_TYPE: TypeAlias = int
WEB_MESSAGE_TYPE: TypeAlias = str

# --- computation_old.py ---
VALUE_TYPE: TypeAlias = float
TIMESTAMP_TYPE: TypeAlias = float  # timestamp in seconds
TIMES_TYPE: TypeAlias = List[TIMESTAMP_TYPE]
VALUES_TYPE: TypeAlias = List[VALUE_TYPE]
DATAPOINT_TYPE: TypeAlias = Tuple[TIMESTAMP_TYPE, VALUE_TYPE]
DATASTREAM_TYPE: TypeAlias = List[DATAPOINT_TYPE]
DATASTREAM_DICT_TYPE: TypeAlias = Dict[str, Any]  # {'times': [], 'values': []}
DATASTREAMS_TYPE: TypeAlias = Dict[DB_UUID_TYPE, DATASTREAM_DICT_TYPE]
VALUE_LOOKUP_TYPE: TypeAlias = Dict[str, VALUE_TYPE]
DETAIL_TYPE: TypeAlias = Dict[str, Optional[str]]  # {'message': None, 'value': 13.2,
#                                                     'time': 1234567890, 'state': running, ...}
DETAILSTREAM_TYPE: TypeAlias = List[DETAIL_TYPE]
INDEX_MAPPING_TYPE: TypeAlias = List[float]
DRIFTS_TYPE: TypeAlias = Dict[str, Any]
# --- Error Handling ---------------------------------------------------------------------------------------------------
# --- config ---
logging.basicConfig(filename=LOGGING_FILE, level=LOGGING_LEVEL, format='%(asctime)s %(levelname)s: %(message)s')


# --- helper functions ---
def handle_error(level: int, message: str, status_code: int, exception: Optional[Exception] = None,
                 data: Optional[dict] = None) -> None:
    """
    Handles an error by logging it and sending it to the frontend.

    :param level:       the level of the error
    :param message:     the message of the error
    :param status_code: the status code of the connection
    :param exception:   the exception connected to the error
    :param data:        the data of the error
    """
    trace = traceback.format_exc() if exception is not None else None
    try:
        # --- variables ---
        log_level_name = logging.getLevelName(level)
        log_message = f"{log_level_name}"
        log_message += f"\nMessage: {message}\nStatus Code: {status_code}"
        if data is not None:
            log_message += f"\nData: {data}"
        if exception is not None:
            log_message += f"\nException: {exception}\nTrace: {trace}"

        # --- log error ---
        if level == logging.CRITICAL:
            logging.critical(log_message)
            # todo inform admin.
        elif level == logging.ERROR:
            logging.error(log_message)
        elif level == logging.WARNING:
            logging.warning(log_message)
        elif level == logging.INFO:
            logging.info(log_message)
        elif level == logging.DEBUG:
            logging.debug(log_message)

        # --- print error for debugging ---
        if DEBUG and level >= PRINT_LEVEL:
            print(log_message)
    except Exception as e:
        print(f"An error occurred while handling an error: {e}")
        print(traceback.format_exc())
        print("THE INITIAL ERROR:")
        print("trace: ", trace)
        print("exception: ", exception)
        print("level: ", level)
        print("message: ", message)
        print("status_code: ", status_code)
        print("data: ", data)
    ########################################


# --- general helper functions -----------------------------------------------------------------------------------------
def merge_sorted_lists(list1, list2, key=lambda x: x):
    """
    Merges two sorted lists into a single sorted list.

    :param list1: First sorted list
    :param list2: Second sorted list
    :param key: Function to apply to list elements for comparison
    :return: Merged sorted list
    """
    i, j = 0, 0
    merged_list = []

    while i < len(list1) and j < len(list2):
        if key(list1[i]) <= key(list2[j]):
            merged_list.append(list1[i])
            i += 1
        else:
            merged_list.append(list2[j])
            j += 1

    # Append remaining elements
    merged_list.extend(list1[i:])
    merged_list.extend(list2[j:])

    return merged_list


def parse_timestamp(timestamp: Union[str, datetime]) -> TIMESTAMP_TYPE:
    """
    parses timestamp from string or datetime to float

    :param timestamp:       the string timestamp or datetime
    :return:                the timestamp in seconds
    """
    if isinstance(timestamp, datetime):
        return TIMESTAMP_TYPE(timestamp.timestamp())
    try:
        return TIMESTAMP_TYPE(datetime.strptime(timestamp, '%Y-%m-%d %H:%M:%S.%f').timestamp())
    except ValueError:
        return TIMESTAMP_TYPE(datetime.strptime(timestamp, '%Y-%m-%d %H:%M:%S').timestamp())


def get_str_stamp(timestamp: TIMESTAMP_TYPE) -> str:
    """
    converts timestamp from float or datetime to string

    :param timestamp:   The timestamp to be converted to a string.
    :return:            The converted string.
    """
    if isinstance(timestamp, float):
        dt_object = datetime.fromtimestamp(timestamp)
    else:
        dt_object = timestamp
    return dt_object.strftime('%Y-%m-%d %H:%M:%S.%f')


def timedelta_to_sting(delta):
    """
    converts timedelta to string.

    :param delta:   The timedelta to be converted to a string.
    :return:        The converted string.
    """
    # --- convert timedelta to seconds ---
    if isinstance(delta, float):
        delta = timedelta(seconds=delta)
    microseconds = delta.total_seconds() * 1e6

    # --- check if delta is zero ---
    if microseconds == 0:
        return "0s"

    # --- compute different time units ---
    years, microseconds = divmod(microseconds, 31536000e6)
    days, microseconds = divmod(microseconds, 86400e6)
    hours, microseconds = divmod(microseconds, 3600e6)
    minutes, microseconds = divmod(microseconds, 60e6)
    seconds, microseconds = divmod(microseconds, 1e6)
    milliseconds, microseconds = divmod(microseconds, 1e3)

    # --- create output string ---
    output = ""
    if years:
        output += f"{int(years)}y "
    if days:
        output += f"{int(days)}d "
    if hours:
        output += f"{int(hours)}h "
    if minutes:
        output += f"{int(minutes)}min "
    if seconds:
        output += f"{int(seconds)}s "
    if milliseconds:
        output += f"{int(milliseconds)}ms "
    if microseconds:
        output += f"{int(microseconds)}µs"

    # --- cleanup ---
    return output.strip()
    ############################


def value_from_lookup(value: str, lookup: VALUE_LOOKUP_TYPE) -> VALUE_TYPE:
    """
    converts string value to float using the lookup.

    :param value:       The value to be converted to a float.
    :param lookup:      The lookup that is used to convert strings to floats.
    :return:            The converted float.
    """
    # --- check if value is in lookup and add it if needed ---
    if value not in lookup:
        lookup[value] = VALUE_TYPE(len(lookup))

    # --- cleanup ---
    return lookup[value]
    ####################

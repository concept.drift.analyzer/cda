#!/usr/bin/python3.10
# --- imports ----------------------------------------------------------------------------------------------------------
# --- general libraries ---
import logging
from asyncio import Queue
from collections import defaultdict
from json import loads, dumps
from os.path import abspath as os_path_abspath, join as os_path_join
from typing import List, Any

from fastapi import FastAPI, Request, Query, APIRouter
from fastapi.responses import JSONResponse, FileResponse, HTMLResponse, StreamingResponse
from fastapi.templating import Jinja2Templates
from icecream import ic

# --- own files ---
from backend.basics import (handle_error, DB_TIMESTAMP_TYPE, DB_VALUE_TYPE, DB_STREAM_NAME_TYPE,
                            DB_DATASTREAM_MAPPING_TYPE, DB_UUID_TYPE)
from backend.data_integration_server import get_as_this_type
from backend.config import WS_PORT
from backend.database import get_streams, get_datastreams
from backend.main import upload_data, get_listener_queues

# --- global variables -------------------------------------------------------------------------------------------------
# initialize App
FRONTEND_DIR = os_path_abspath('../frontend')
static_folder = os_path_join(FRONTEND_DIR, 'static')
templates = Jinja2Templates(directory=os_path_join(FRONTEND_DIR, 'templates'))
app = FastAPI()

# manage port rerouting
router = APIRouter()


# --- helper functions -------------------------------------------------------------------------------------------------
def format_json(message: Any, status_code: int) -> JSONResponse:
    return JSONResponse(content={'message': message, 'status_code': status_code}, status_code=status_code)


# --- main functions ---------------------------------------------------------------------------------------------------

@router.post('/upload_data')
async def upload_data_route(request: Request):
    try:
        # --- check uuid input ---
        form_data = await request.form()
        uuid, ret_code = get_as_this_type(form_data.get('uuid'), DB_UUID_TYPE)
        if ret_code != 200:
            return format_json(uuid, ret_code)
        # --- check datastream_mapping ---
        dsm_str = loads(form_data.get('datastream_mapping'))
        if not isinstance(dsm_str, dict):
            return format_json("datastream_mapping is not a dictionary", 400)
        datastream_mapping: DB_DATASTREAM_MAPPING_TYPE = defaultdict(list)
        for key, datastreams in dsm_str.items():
            key, ret_code = get_as_this_type(key, DB_STREAM_NAME_TYPE)
            if ret_code != 200:
                return format_json(key, ret_code)
            if not isinstance(datastreams, list):
                return format_json("datastream_mapping[key] is not a list", 400)
            for timestamp, value in datastreams:
                timestamp, ret_code = get_as_this_type(timestamp, DB_TIMESTAMP_TYPE)
                if ret_code != 200:
                    return format_json(timestamp, ret_code)
                value, ret_code = get_as_this_type(value, DB_VALUE_TYPE)
                if ret_code != 200:
                    return format_json(value, ret_code)
                datastream_mapping[key].append((timestamp, value))
        # --- upload data ---
        message, status_code = await upload_data(uuid, datastream_mapping)
        return format_json(message, status_code)
    except Exception as e:
        # Catch any exception that would otherwise cause a server crash
        handle_error(logging.ERROR, f"An error occurred: {e}", 500, e, {"request": request})
        return format_json('Invalid input or server error', 400)
    ###################################


@router.get("/get_datastreams")
def get_datastreams_route(stream_name: str = Query(), value_filter: str = Query(default=""),
                          max_length: int = Query(default=0)):
    try:
        parsed_value_filter: List[float] = loads("[" + value_filter + "]")
        ic(stream_name, parsed_value_filter, max_length)
        datastreams, sorted_uuids, is_discrete = get_datastreams(stream_name, [], True, parsed_value_filter, max_length, False)

        initial_data = {
            "type": 'initial',
            "sorted_uuids": sorted_uuids,
            "data": datastreams,
            "discreteness": is_discrete
        }
        return format_json(initial_data, 200)
    except Exception as e:
        handle_error(logging.ERROR, f"An error occurred: {e}", 500, e)
        return format_json('Invalid input or server error', 400)
    ###############################


@router.get("/listen")
async def listen(stream_name: str = Query(), value_filter: str = Query(default=""), max_length: int = Query(default=0)):
    try:
        # --- variables ---
        own_queue = Queue()
        get_listener_queues()[stream_name].append(own_queue)
        parsed_value_filter: List[float] = loads("[" + value_filter + "]")

        # --- helper functions ---
        async def data_receiver(queue: Queue):
            # --- send initial data ---
            datastreams, sorted_uuids, is_discrete = get_datastreams(stream_name, [], True,
                                                                     parsed_value_filter, max_length, True)
            initial_data = {
                "type": 'initial',
                "sorted_uuids": sorted_uuids,
                "data": datastreams,
                "discreteness": is_discrete
            }
            yield f"data: {dumps(initial_data)}\n\n"

            # --- send continuous data ---
            while True:
                try:
                    data = await queue.get()
                    to_send = dumps(data)
                except Exception as eee:
                    handle_error(logging.ERROR, f"An error occurred: {eee}", 500, eee)
                else:
                    yield f"data: {to_send}\n\n"

        # --- cleanup ---
        return StreamingResponse(data_receiver(own_queue), media_type="text/event-stream")
    except Exception as e:
        handle_error(logging.ERROR, f"An error occurred: {e}", 500, e)
        return format_json('Invalid input or server error', 400)
    ###############################################


@router.get('/get_streams', response_model=dict)
async def get_streams_route():
    try:
        return JSONResponse(content={'message': 'successful database read.',
                                     'status_code': 200,
                                     'data': get_streams()})
    except Exception as e:
        handle_error(level=logging.ERROR, message=f"An error occurred: {e}", status_code=500, exception=e)
        return JSONResponse(content={
            'message': "Error while reading database.",
            'status_code': 500,
            'data': None
        }, status_code=500)


# --- frontend GUI functions ---
@router.get("/", response_class=HTMLResponse)
async def get_home(request: Request):
    return templates.TemplateResponse("main.html", {"request": request})


@router.get("/static/{filename:path}", include_in_schema=False)
async def custom_static(filename: str):
    return FileResponse(os_path_join(static_folder, filename))


app.include_router(router)
# --- main -------------------------------------------------------------------------------------------------------------
if __name__ == "__main__":
    try:
        print(f"Webserver started on port {WS_PORT}")
        import uvicorn
        uvicorn.run(app, host="::", port=WS_PORT, http="h11")
    except Exception as ee:
        handle_error(logging.CRITICAL, 'Server error', 500, ee)

# --- imports ----------------------------------------------------------------------------------------------------------
# --- other libraries ---
import logging
from typing import List

# --- Deployment Constants ---------------------------------------------------------------------------------------------
# --- webserver ---
WS_PORT: int = 54165
PORT_REROUTING: str = ''  # '/ports/54165'                                       # set to '' if no port rerouting exists

# --- data integration server ---
DIS_PORT: int = 54166

# --- database ---
DATABASE: str = "../data/concept_drift_analyzer.db"
POSSIBLE_STATES: List[str] = ['starting', 'running', 'stopping', 'stopped', 'error', 'active', 'unavailable',
                              'Opened', 'Closed', 'Intermediate']

# --- logging ---
LOGGING_FILE: str = "../data/concept_drift_analyzer.log"
LOGGING_LEVEL: int = logging.DEBUG
DEBUG: bool = True                                                                          # set to False in production
PRINT_LEVEL: int = logging.WARNING

# --- computation / drift detection ---

USE_FILTER: bool = True  # filters out false values from 'measurement' (999.99)
#                          can be configured in computation_old.py/get_filter()  // todo NO this is a config file. it should be configurable here!!!
DURATION_TOLERANCE: float = 0.2                                 # 0.2 = 20% tolerance according to the average duration.
RAM_USAGE_TOLERANCE: float = 0.9                          # 0.9 = 90%. Reduces Ram usage, if more ram is currently used.
#                                              values 1 is the most performant value. value 0 reduces ram usage maximal.

# --- imports ----------------------------------------------------------------------------------------------------------
# --- general libraries ---
import logging
from collections import defaultdict
from typing import Tuple, List, Dict
from asyncio import Queue

from icecream import ic

# --- own files ---
from backend.database import get_cursor, release_cursor, insert_datastream, db_locked
from backend.basics import (handle_error, DB_UUID_TYPE, WEB_MESSAGE_TYPE, STATUS_CODE_TYPE, DB_DATASTREAM_MAPPING_TYPE,
                            DB_STREAM_NAME_TYPE, parse_timestamp)

# --- global variables -------------------------------------------------------------------------------------------------
listener_queues: Dict[DB_STREAM_NAME_TYPE, List[Queue]] = defaultdict(list)


# --- helper functions -------------------------------------------------------------------------------------------------
def get_listener_queues() -> Dict[DB_STREAM_NAME_TYPE, List[Queue]]:
    """
    Getter for the listener queues.

    :return: The listener queues.
    """
    return listener_queues
    ########################


# --- main functions ---------------------------------------------------------------------------------------------------
async def upload_data(uuid: DB_UUID_TYPE, datastream_mapping: DB_DATASTREAM_MAPPING_TYPE) \
        -> Tuple[WEB_MESSAGE_TYPE, STATUS_CODE_TYPE]:
    """
    Uploads data to the database.
    Makes sure only one instance can upload at a time via db_locked().

    :param uuid:                Instance.uuid
    :param datastream_mapping:  {Stream.stream_name: [(Data.timestamp, Data.value), ...], ...}
    :return: (return_message, return_code)
    """
    # --- variables ---
    cursor = None
    to_send: Dict[DB_STREAM_NAME_TYPE, List[Dict]] = defaultdict(list)       # stream_name None indicates "all streams"
    new_datastreams = {}                                                     # {stream_name: min_timestamp, ...}

    # --- upload data to database ---
    try:
        cursor = get_cursor()
        async with db_locked():
            for stream_name, datastream in datastream_mapping.items():
                discreteness, did_already_exist = insert_datastream(cursor, uuid, stream_name, datastream)
                if not did_already_exist:
                    new_datastreams[stream_name] = parse_timestamp(datastream[0][0])
                datastream = {'time': list(map(lambda x: parse_timestamp(x[0]), datastream)),
                              'value': list(map(lambda x: x[1], datastream))}
                to_send[stream_name].append({
                    "type": 'new_data',
                    "data": {uuid: datastream},
                    "discreteness": discreteness
                })
            cursor.connection.commit()

    # --- error handling ---
    except Exception as e:
        handle_error(logging.ERROR, str(e), 500, e, {'uuid': uuid, 'datastream_mapping': datastream_mapping})
        return str(e), 500

    # --- cleanup ---
    finally:
        release_cursor(cursor)

    # --- broadcast data ---
    # send to all streams, if new datastreams appear
    if new_datastreams:
        for queue_list in listener_queues.values():
            for queue in queue_list:
                try:
                    await queue.put({"type": 'new_datastreams', "uuid": uuid, "data": new_datastreams})
                except Exception as e:
                    handle_error(logging.WARNING, "some error occurred while sending to queue", 500, e, new_datastreams)

    # send to individual streams
    handle_error(logging.INFO, "todo delete me", 0, None, to_send)
    for stream_name, dict_list in to_send.items():
        for queue in listener_queues[stream_name]:
            for element in dict_list:
                try:
                    await queue.put(element)
                except Exception as e:
                    handle_error(logging.WARNING, "some error occurred while sending to queue", 500, e, element)

    # --- return ---
    return 'upload successful', 200
    ######################
